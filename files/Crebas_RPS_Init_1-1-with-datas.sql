-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 06, 2019 at 04:42 PM
-- Server version: 5.5.59-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rps`
--

-- --------------------------------------------------------

--
-- Table structure for table `mood`
--

CREATE TABLE IF NOT EXISTS `mood` (
  `id_mood` int(11) NOT NULL AUTO_INCREMENT,
  `type_mood` varchar(255) NOT NULL,
  PRIMARY KEY (`id_mood`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mood`
--

INSERT INTO `mood` (`id_mood`, `type_mood`) VALUES
(1, 'green'),
(2, 'yellow'),
(3, 'orange'),
(4, 'red');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `id_rubric` int(11) NOT NULL,
  `order_question` int(11) NOT NULL,
  `wording_question` varchar(300) NOT NULL,
  `active_comment_question` tinyint(1) DEFAULT '1',
  `active_question` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_question`),
  KEY `question_rubric_FK` (`id_rubric`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id_question`, `id_rubric`, `order_question`, `wording_question`, `active_comment_question`, `active_question`) VALUES
(3, 4, 1, 'Avez-vous à vivre des situations de tension avec les publics liés à des mécontentements , à gérer des altercations verbales, physiques ? ', 1, 1),
(4, 4, 2, ' Devez-vous faire face à des comportements d''incivilités (utilisation du téléphone, tabac, vidéos...), à des dégradations, des vols ? ', 0, 0),
(5, 4, 3, ' Etes-vous confronté à des stagiaires ayant des consommations problématiques de substances addictives (drogues, alcool...) ? ', 1, 1),
(6, 4, 4, 'Votre activité vous parait-elle éprouvante émotionnellement, du fait de situations de souffrance (physique, psychologique ou sociale) exprimées par les personnes accueillies ? ', 1, 1),
(7, 4, 5, 'Etes-vous tenu.e de "faire bonne figure" en adoptant une attitude bienveillante et disponible en toutes circonstances ? ', 0, 1),
(8, 5, 6, 'Avez-vous le sentiment de faire partie d''un collectif soudé, dans lequel règne un climat de confiance, de respect mutuel, où les relations sont propices aux échanges et à la solidarité ? ', 1, 1),
(9, 5, 7, 'Avez-vous observé chez vos collègues, des comportements de type repli sur soi, plaintes, irritabilité, démotivation, propos cyniques... ?', 1, 1),
(10, 5, 8, 'Vous est-il arrivé, en raison de problématiques de travail, de devoir prendre des médicaments pour pouvoir vous rendre au travail et tenir le coup ?', 1, 1),
(11, 5, 9, ' Vous sentez-vous à l''aise pour exposer une difficulté à votre hiérarchique ?', 1, 1),
(12, 5, 10, ' Avez-vous le sentiment d''obtenir de votre supérieur hiérarchique l''écoute, les ressources et les réponses dont vous avez besoin (disponibilité, appui, moyens techniques, logistiques, humains...) ?', 1, 1),
(13, 5, 11, 'Vous arrive-t-il d''être affecté par des messages reçus par courriel, de la part de collègues ou de la hiérarchie?', 1, 1),
(14, 6, 14, 'Les marques de reconnaissance de votre travail (rétribution, promotion, reclassement, éloges félicitations, encouragements…) vous semblent-elles suffisantes ?', 1, 1),
(15, 6, 15, ' Obtenez-vous les formations que vous demandez en lien avec les compérences que vous devez mobiliser ? ', 1, 1),
(16, 6, 16, 'Avez-vous observé des situations d''inéquité de traitement entre salariés (recrutement, promotion, évolution, moyens…) ?', 1, 1),
(17, 6, 17, 'Les outils RH d''accompagnement des salariés facilitent-ils la mobilité professionnelle ou le reclassement, dans les situations de restrictions médicales ?', 1, 1),
(18, 6, 18, 'Votre poste actuel et vos missions correspondent-ils à ce qui figure sur votre contrat de travail ?', 1, 1),
(19, 5, 13, 'Vous sentez-vous autorisé à exprimer votre point de vue, en toute situation ? ', 1, 1),
(20, 5, 12, 'Avez-vous connaissance de situations de frictions entre services, salariés ou encore de cas de conflit exacerbé (actes hostiles, propos blessants, attitudes méprisantes, mises à l''écart, comportements individualistes…) ? ', 1, 1),
(22, 8, 19, 'svsdfv', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `question__suggestion__user`
--

CREATE TABLE IF NOT EXISTS `question__suggestion__user` (
  `id_question` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_suggestion` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  KEY `question__suggestion__user_question_FK` (`id_question`),
  KEY `question__suggestion__user_user0_FK` (`id_user`),
  KEY `question__suggestion__user_suggestion1_FK` (`id_suggestion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question__suggestion__user`
--

INSERT INTO `question__suggestion__user` (`id_question`, `id_user`, `id_suggestion`, `comment`) VALUES
(3, NULL, 5, ''),
(3, NULL, 6, ''),
(3, NULL, 7, ''),
(3, NULL, 8, ''),
(4, NULL, 9, ''),
(4, NULL, 10, ''),
(4, NULL, 11, ''),
(4, NULL, 12, ''),
(5, NULL, 78, ''),
(5, NULL, 79, ''),
(5, NULL, 80, ''),
(5, NULL, 81, ''),
(6, NULL, 82, ''),
(6, NULL, 83, ''),
(6, NULL, 84, ''),
(6, NULL, 85, ''),
(7, NULL, 86, ''),
(7, NULL, 87, ''),
(7, NULL, 88, ''),
(7, NULL, 89, ''),
(8, NULL, 90, ''),
(8, NULL, 91, ''),
(8, NULL, 92, ''),
(8, NULL, 93, ''),
(9, NULL, 94, ''),
(9, NULL, 95, ''),
(9, NULL, 96, ''),
(9, NULL, 97, ''),
(10, NULL, 98, ''),
(10, NULL, 99, ''),
(10, NULL, 100, ''),
(10, NULL, 101, ''),
(11, NULL, 102, ''),
(11, NULL, 103, ''),
(11, NULL, 104, ''),
(11, NULL, 105, ''),
(12, NULL, 106, ''),
(12, NULL, 107, ''),
(12, NULL, 108, ''),
(12, NULL, 109, ''),
(13, NULL, 110, ''),
(13, NULL, 111, ''),
(13, NULL, 112, ''),
(13, NULL, 113, ''),
(14, NULL, 114, ''),
(14, NULL, 115, ''),
(14, NULL, 116, ''),
(14, NULL, 117, ''),
(15, NULL, 118, ''),
(15, NULL, 119, ''),
(15, NULL, 120, ''),
(15, NULL, 121, ''),
(16, NULL, 122, ''),
(16, NULL, 123, ''),
(16, NULL, 124, ''),
(16, NULL, 125, ''),
(17, NULL, 126, ''),
(17, NULL, 127, ''),
(17, NULL, 128, ''),
(17, NULL, 129, ''),
(18, NULL, 130, ''),
(18, NULL, 131, ''),
(18, NULL, 132, ''),
(18, NULL, 133, ''),
(19, NULL, 134, ''),
(19, NULL, 135, ''),
(19, NULL, 136, ''),
(19, NULL, 137, ''),
(20, NULL, 138, ''),
(20, NULL, 139, ''),
(20, NULL, 140, ''),
(20, NULL, 141, '');

-- --------------------------------------------------------

--
-- Table structure for table `rubric`
--

CREATE TABLE IF NOT EXISTS `rubric` (
  `id_rubric` int(11) NOT NULL AUTO_INCREMENT,
  `id_theme` int(11) NOT NULL,
  `order_rubric` int(11) NOT NULL,
  `wording_rubric` varchar(255) NOT NULL,
  `active_rubric` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_rubric`),
  KEY `rubric_theme_FK` (`id_theme`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `rubric`
--

INSERT INTO `rubric` (`id_rubric`, `id_theme`, `order_rubric`, `wording_rubric`, `active_rubric`) VALUES
(4, 1, 1, 'Relations aux publics : stagiaires, clients, fourniseurs ...', 1),
(5, 10, 3, 'Qualité des relations entre salariés et soutien de la hiérarchie', 1),
(6, 10, 4, ' Reconnaissance du travail et des compétences', 1),
(7, 1, 2, 'Petite rubric de test', 0),
(8, 11, 5, 'Tagadatsoin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `suggestion`
--

CREATE TABLE IF NOT EXISTS `suggestion` (
  `id_suggestion` int(11) NOT NULL AUTO_INCREMENT,
  `id_mood` int(11) NOT NULL,
  `wording_suggestion` varchar(30) DEFAULT NULL,
  `active_suggestion` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_suggestion`),
  KEY `suggestion_mood_FK` (`id_mood`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=146 ;

--
-- Dumping data for table `suggestion`
--

INSERT INTO `suggestion` (`id_suggestion`, `id_mood`, `wording_suggestion`, `active_suggestion`) VALUES
(5, 1, 'Toujours', 1),
(6, 2, 'Souvent', 1),
(7, 3, 'Parfois', 1),
(8, 4, 'Jamais', 1),
(9, 1, 'toujours', 1),
(10, 2, 'Souvant', 0),
(11, 3, 'parfois', 0),
(12, 4, 'Jamais', 1),
(78, 1, 'Toujours', 1),
(79, 2, 'Souvent', 1),
(80, 3, 'Parfois', 1),
(81, 4, 'Jamais', 1),
(82, 1, 'Toujours', 1),
(83, 2, 'Souvent', 1),
(84, 3, 'Parfois', 1),
(85, 4, 'Jamais', 1),
(86, 1, 'Toujours', 1),
(87, 2, 'Souvent', 1),
(88, 3, 'Parfois', 1),
(89, 4, 'Jamais', 1),
(90, 1, 'Toujours', 1),
(91, 2, 'Souvent', 1),
(92, 3, 'Parfois', 1),
(93, 4, 'Jamais', 1),
(94, 1, 'Toujours', 1),
(95, 2, 'Souvent', 1),
(96, 3, 'Parfois', 1),
(97, 4, 'Jamais', 1),
(98, 1, 'Toujours', 1),
(99, 2, 'Souvent', 1),
(100, 3, 'Parfois', 1),
(101, 4, 'Jamais', 1),
(102, 1, 'Toujours', 1),
(103, 2, 'Souvent', 1),
(104, 3, 'Parfois', 1),
(105, 4, 'Jamais', 1),
(106, 1, 'Toujours', 1),
(107, 2, 'Souvent', 1),
(108, 3, 'Parfois', 1),
(109, 4, 'Jamais', 1),
(110, 1, 'Toujours', 1),
(111, 2, 'Souvent', 1),
(112, 3, 'Parfois', 1),
(113, 4, 'Jamais', 1),
(114, 1, 'Toujours', 1),
(115, 2, 'Souvent', 1),
(116, 3, 'Parfois', 1),
(117, 4, 'Jamais', 1),
(118, 1, 'Toujours', 1),
(119, 2, 'Souvent', 1),
(120, 3, 'Parfois', 1),
(121, 4, 'Jamais', 1),
(122, 1, 'Toujours', 1),
(123, 2, 'Souvent', 1),
(124, 3, 'Parfois', 1),
(125, 4, 'Jamais', 1),
(126, 1, 'Toujours', 1),
(127, 2, 'Souvent', 1),
(128, 3, 'Parfois', 1),
(129, 4, 'Jamais', 1),
(130, 1, 'Toujours', 1),
(131, 2, 'Souvent', 1),
(132, 3, 'Parfois', 1),
(133, 4, 'Jamais', 1),
(134, 1, 'Toujours', 1),
(135, 2, 'Souvent', 1),
(136, 3, 'Parfois', 1),
(137, 4, 'Jamais', 1),
(138, 1, 'Toujours', 1),
(139, 2, 'Souvent', 1),
(140, 3, 'Parfois', 1),
(141, 4, 'Jamais', 1),
(142, 1, 'Toujours', 1),
(143, 2, 'Souvent', 1),
(144, 3, 'Parfois', 1),
(145, 4, 'Jamais', 1);

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `id_survey` int(11) NOT NULL AUTO_INCREMENT,
  `wording_survey` varchar(255) NOT NULL,
  `active_survey` tinyint(1) DEFAULT '1',
  `date_survey` date DEFAULT NULL,
  PRIMARY KEY (`id_survey`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`id_survey`, `wording_survey`, `active_survey`, `date_survey`) VALUES
(1, 'Test de positionnement 2019', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id_theme` int(11) NOT NULL AUTO_INCREMENT,
  `id_survey` int(11) NOT NULL,
  `order_theme` int(11) NOT NULL,
  `wording_theme` varchar(255) NOT NULL,
  `description_theme` varchar(300) DEFAULT NULL,
  `active_theme` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_theme`),
  KEY `theme_survey_FK` (`id_survey`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `theme`
--

INSERT INTO `theme` (`id_theme`, `id_survey`, `order_theme`, `wording_theme`, `description_theme`, `active_theme`) VALUES
(1, 1, 1, 'Exigences Emotionnelles\r\n ', '...', 1),
(10, 1, 2, 'Rapports sociaux au travail dégradés ', ' ...', 1),
(11, 1, 3, 'test', '...', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_type` int(11) NOT NULL,
  `password_user` varchar(255) DEFAULT NULL,
  `name_user` varchar(255) DEFAULT NULL,
  `email_user` varchar(255) DEFAULT NULL,
  `date_user` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_submited_survey` date DEFAULT NULL,
  `active_user` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_user`),
  KEY `user_user_type_FK` (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_type`, `password_user`, `name_user`, `email_user`, `date_user`, `date_submited_survey`, `active_user`) VALUES
(1, 1, 'U2FsdGVkX1+dYyual/WpVpcq9blrRlkq', 'Raymonde Ricourt', 'r.ricourt@afpa.com', '2019-06-03 09:33:14', NULL, 1),
(2, 1, 'U2FsdGVkX1/dAo/zuN4B7kBobUs1ZoTO', 'Jean-Jacque Pagan', 'jijou@afpa.com', '2019-06-06 06:23:07', NULL, 1),
(3, 2, 'U2FsdGVkX18HCqfUtRuYTR9fAYX8QR6d', 'Cyril Vassallo', 'cyrilvssll34@gmail.com', '2019-06-06 06:24:05', NULL, 1),
(4, 2, 'U2FsdGVkX1/p6iuP4OwNtEVCa4CSEKmG', 'Charles Lang', 'c.lang@gmail.fr', '2019-06-06 06:24:54', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(15) DEFAULT NULL,
  `category_type` varchar(120) DEFAULT NULL,
  `right_type` tinyint(4) NOT NULL,
  `active_type` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id_type`, `user_type`, `category_type`, `right_type`, `active_type`) VALUES
(1, 'admin', 'Full admin', 1, 1),
(2, 'admin', 'Limited Admin', 2, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_rubric_FK` FOREIGN KEY (`id_rubric`) REFERENCES `rubric` (`id_rubric`);

--
-- Constraints for table `question__suggestion__user`
--
ALTER TABLE `question__suggestion__user`
  ADD CONSTRAINT `question__suggestion__user_question_FK` FOREIGN KEY (`id_question`) REFERENCES `question` (`id_question`),
  ADD CONSTRAINT `question__suggestion__user_suggestion1_FK` FOREIGN KEY (`id_suggestion`) REFERENCES `suggestion` (`id_suggestion`),
  ADD CONSTRAINT `question__suggestion__user_user0_FK` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `rubric`
--
ALTER TABLE `rubric`
  ADD CONSTRAINT `rubric_theme_FK` FOREIGN KEY (`id_theme`) REFERENCES `theme` (`id_theme`);

--
-- Constraints for table `suggestion`
--
ALTER TABLE `suggestion`
  ADD CONSTRAINT `suggestion_mood_FK` FOREIGN KEY (`id_mood`) REFERENCES `mood` (`id_mood`);

--
-- Constraints for table `theme`
--
ALTER TABLE `theme`
  ADD CONSTRAINT `theme_survey_FK` FOREIGN KEY (`id_survey`) REFERENCES `survey` (`id_survey`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_user_type_FK` FOREIGN KEY (`id_type`) REFERENCES `user_type` (`id_type`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
