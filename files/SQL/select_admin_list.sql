SELECT id_user, name_user, email_user, active_user, right_type
FROM user INNER JOIN user_type
ON user.id_type = user_type.id_type
WHERE user_type = 'admin'
ORDER BY right_type