SELECT 
    survey.id_survey, survey.wording_survey, survey.date_survey, 
    theme.id_theme, theme.order_theme, theme.wording_theme, theme.description_theme, theme.active_theme,
    rubric.id_rubric, rubric.order_rubric, rubric.wording_rubric, rubric.active_rubric, 
    question.id_question, question.order_question, question.wording_question, question.active_comment_question, question.active_question,question__suggestion__user.id_suggestion, 
    suggestion.id_suggestion, suggestion.id_mood, suggestion.wording_suggestion, suggestion.active_suggestion, 
    mood.type_mood
FROM 
    survey, theme, rubric, question, question__suggestion__user, suggestion, mood 
WHERE 
    survey.id_survey= @id_survey
and 
    question__suggestion__user.id_suggestion=suggestion.id_suggestion
and 
    question__suggestion__user.id_user IS NULL
and 
    suggestion.active_suggestion=1
and 
    question.id_question=question__suggestion__user.id_question
and 
    question.active_question=1
and 
    rubric.id_rubric=question.id_rubric
and 
    rubric.active_rubric=1
and 
    theme.id_theme=rubric.id_theme
and 
    theme.active_theme=1
and 
    survey.id_survey=theme.id_survey
and 
    suggestion.id_mood=mood.id_mood
order by 
    theme.order_theme, 
    rubric.order_rubric, 
    question.order_question, 
    suggestion.id_suggestion