#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: survey
#------------------------------------------------------------

CREATE TABLE survey(
        id_survey      Int  Auto_increment  NOT NULL ,
        wording_survey Varchar (255) NOT NULL ,
        active_survey  TinyINT default 1 ,
        date_survey    Date
	,CONSTRAINT survey_PK PRIMARY KEY (id_survey)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: theme
#------------------------------------------------------------

CREATE TABLE theme(
        id_theme          Int  Auto_increment  NOT NULL ,
        id_survey         Int NOT NULL ,
        order_theme       Int NOT NULL ,
        wording_theme     Varchar (255) NOT NULL ,
        description_theme Varchar (300) ,
        active_theme      TinyINT default 1 
	,CONSTRAINT theme_PK PRIMARY KEY (id_theme)

	,CONSTRAINT theme_survey_FK FOREIGN KEY (id_survey) REFERENCES survey(id_survey)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: rubric
#------------------------------------------------------------

CREATE TABLE rubric(
        id_rubric      Int  Auto_increment  NOT NULL ,
        id_theme       Int NOT NULL ,
        order_rubric   Bool NOT NULL ,
        wording_rubric Varchar (255) NOT NULL ,
        active_rubric  TinyINT default 1
	,CONSTRAINT rubric_PK PRIMARY KEY (id_rubric)

	,CONSTRAINT rubric_theme_FK FOREIGN KEY (id_theme) REFERENCES theme(id_theme)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: question
#------------------------------------------------------------

CREATE TABLE question(
        id_question             Int  Auto_increment  NOT NULL ,
        id_rubric               Int NOT NULL ,
        order_question          Int NOT NULL ,
        wording_question        Varchar (300) NOT NULL ,
        active_comment_question TinyINT NOT NULL default 1 ,
        active_question         TinyINT NOT NULL default 1
	,CONSTRAINT question_PK PRIMARY KEY (id_question)

	,CONSTRAINT question_rubric_FK FOREIGN KEY (id_rubric) REFERENCES rubric(id_rubric)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: mood
#------------------------------------------------------------

CREATE TABLE mood(
        id_mood   Int  Auto_increment  NOT NULL ,
        type_mood Varchar (255) NOT NULL
	,CONSTRAINT mood_PK PRIMARY KEY (id_mood)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: suggestion
#------------------------------------------------------------

CREATE TABLE suggestion(
        id_suggestion      Int  Auto_increment  NOT NULL ,
        id_mood            Int NOT NULL ,
        wording_suggestion Varchar (30) ,
        active_suggestion  TinyINT default 1
	,CONSTRAINT suggestion_PK PRIMARY KEY (id_suggestion)

	,CONSTRAINT suggestion_mood_FK FOREIGN KEY (id_mood) REFERENCES mood(id_mood)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user_type
#------------------------------------------------------------

CREATE TABLE user_type(
        id_type       Int  Auto_increment  NOT NULL ,
        user_type     Varchar (15) ,
        category_type Varchar (120) ,
        right_type    TinyINT NOT NULL ,
        active_type   TinyINT default 1
	,CONSTRAINT user_type_PK PRIMARY KEY (id_type)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user
#------------------------------------------------------------

CREATE TABLE user(
        id_user              Int  Auto_increment  NOT NULL ,
        id_type              Int NOT NULL ,
        password_user        Varchar (255) ,
        name_user            Varchar (255) ,
        email_user           Varchar (255) ,
        date_user            Date ,
        date_submited_survey Date ,
        active_user          TinyINT default 1
	,CONSTRAINT user_PK PRIMARY KEY (id_user)

	,CONSTRAINT user_user_type_FK FOREIGN KEY (id_type) REFERENCES user_type(id_type)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: question__suggestion__user
#------------------------------------------------------------

CREATE TABLE question__suggestion__user(
        id_question   Int NOT NULL ,
        id_user       Int ,
        id_suggestion Int NOT NULL ,
        comment       Varchar (500) NOT NULL

	,CONSTRAINT question__suggestion__user_question_FK FOREIGN KEY (id_question) REFERENCES question(id_question)
	,CONSTRAINT question__suggestion__user_user0_FK FOREIGN KEY (id_user) REFERENCES user(id_user)
	,CONSTRAINT question__suggestion__user_suggestion1_FK FOREIGN KEY (id_suggestion) REFERENCES suggestion(id_suggestion)
)ENGINE=InnoDB;

