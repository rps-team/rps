<?php
/**
 * updated 24/08/2019
 */
require_once "admin_service.php";
require_once "utils.php";
/**
 * Class Forgot | file forgot.php
 *
 * In this class, we find all about Forgot page 
 *
 * List of classes needed for this class
 * admin_service.php
 * utils.php
 *
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Forgot extends Admin_service	{

	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * execute main function
	 * 
	 */
		parent::__construct();

		// execute main function
		$this->main();
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * Execute when constructor is called
	 * decrypt id from url parameter (crypted_key token)
	 * if password valid update user password else set error message to display to the view
	 * 
	 * TODO // decrypt idUser (find why Utils::decrypter not working)
	 * TODO // forgot.js check passwords fields
	 */
	public function main()	{
		if(isset($this->VARS_HTML['crypted_key'])&& !empty($this->VARS_HTML['crypted_key']) ){

			$this->result['crypted_key'] = $this->VARS_HTML['crypted_key'];
			error_log("crypted_key: ". $this->VARS_HTML['crypted_key']);
			// $keyToDecrypt = $this->VARS_HTML['crypted_key']; 
			// $IdUser = Utils::decrypter('rps042019.',$keyToDecrypt);
			$idUser = $this->VARS_HTML['crypted_key'];
			error_log('id_user: '.$idUser);
		}
		else{
			$passwordForgot = $this->VARS_HTML['password_forgot'];
			$passwordConfirmForgot = $this->VARS_HTML['password_confirm_forgot'];
			$passWordState = $this->controlPassword($passwordForgot, $passwordConfirmForgot);
			if($passWordState == 0){
				$this->updateAdminPassword($passwordForgot, $passwordConfirmForgot);
			}
			else{
				$this->setMessageAndCodeError($passwordState);
			}
		}
	}



	/**
	* Control if the password exist and if the given password is valid
	*
	* @param string $passwordToCompare
	* @param string $confirmToCompare
	* @return void
	*/
	private function controlPassword($passwordToCompare, $confirmToCompare){
		error_log('controlPassword function started');
		$nbState ='';
		$isPasswordEmpty = !(isset($passwordToCompare)) || $passwordToCompare == '';
		$isConfirmEmpty =!(isset($confirmToCompare)) || $confirmToCompare == '';
		if(!$isPasswordEmpty || !$isConfirmEmpty)
		{
			$isEqual = $passwordToCompare == $confirmToCompare;
			if($isEqual)
			{
				$pathern = '/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!\.*$@%_])([-+!\.*$@%_\w]{8,15})$/';
				$isPasswordValid = preg_match($pathern, $confirmToCompare);
				if($isPasswordValid)
				{
					$nbState ='0';
					
				}
				else
				{
					$nbState ='6';
				}
			}
			else
			{
				$nbState ='5';
			}
		}
		else
		{
			$nbState ='4';	
		} 
		error_log('isPasswordValid:'.$nbState);
		return $nbState;
	}
	
	/**
	* Called by main function
	* set message in result['code_error'] for the view
	*
	* @param integer $nbState
	* @return void
	*/
	private function setMessageAndCodeError($nbState){
		if($nbState == 6){
			$this->result['message_error'] = 'Password invalide';
			$this->result['code_error'] = 6;
		}
		else if($nbState == 5){
			$this->result['message_error'] = 'Les passwords sont différents';
			$this->result['code_error'] = 5;
		}
		else if($nbState == 4){
			$this->result['message_error'] = '*Un des champs est vide';
			$this->result['code_error'] = 4;
		}		
	}
	
//End of class
}

?>
