
<?php
/**
 * updated 24/08/2019
 */
require_once 'initialize.php';
require_once 'utils.php';

/**
 * Class Administer_service | file administer_service.php
 *
 * In this class, we find all functions about request 
 * for Admin page and Administer page
 * Admin class,
 * Admin_ajax class, 
 * Administer class ,
 * Administer_ajax class, 
 * forgot class,
 * home class 
 * all extends this class
 *
 * List of classes needed for this class
 * initialize.php
 * utils.php
 *
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Admin_service extends Initialize	{

	/**
	 * public $result is used to store all data needed for HTML Templates and ajax_json: 
	 * admin.html, 
	 * admin_ajax.html(json), 
	 * administer.html,
	 * administer_ajax.html(json)
	 * forgot.html
	 * home.html
	 * @var array $result
	 * @var object $oBdd
	 */
	public $result;
	private $oBdd;
	
	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * init variables result
	 *
	 * execute main function
	 * 
	 * charge the title for this class to send to header_admin.html using result 
	 */
		$this->oBdd= parent::__construct();
		// init variables result
		$this->result= [];
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}
	
	/**
	 * SELECT the list of administrator 
	 */
	public function adminList(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_admin_list.sql';
		$this->result['admin_list']= $this->oBdd->getSelectDatas($spathSQL, array());
	}
	
	/**
	 * SELECT administrator by email
	 */
	public function selectAdminForgotPassord(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_admin_forgot_pass.sql';
		$this->result['select_admin_forgot_pass']= $this->oBdd->getSelectDatas($spathSQL, array(
																				'email_user' => $this->VARS_HTML['email_user_forgot']
																				));																	
	}
	
	/**
	 * SELECT active survey
	 */
	function selectActiveSurveyTitle()	{
		
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_active_survey_title_public.sql';
		$this->result['active_survey_title']= $this->oBdd->getSelectDatas($spathSQL, array());
	}
	
	/**
	 * SELECT last Insert User
	 *
	 * @param integer $lastInsertId
	 * @return void
	 */
	function selectLastUser($lastInsertId){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_last_user.sql';
		$this->result['last_user']= $this->oBdd->getSelectDatas($spathSQL, array(
																							'id_user' => $lastInsertId
																			));
	}

	/**
	 * SELECT a single administrator
	 */
	public function selectSingleAdmin(){
		error_log('REQUEST SINGLE ADMIN');
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_single_admin.sql';
		$cryptedPassword = Utils::crypterSSL($this->VARS_HTML['email_user'], $this->VARS_HTML['password_user']);
		$this->result['select_admin']= $this->oBdd->getSelectDatas($spathSQL, array(
																				'email_user' => $this->VARS_HTML['email_user'],
																				'password_user' => $cryptedPassword
																				));																	
	}

	public function selectExistingEmail(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_existing_email.sql';
		$this->result['select_existing_email']= $this->oBdd->getSelectDatas($spathSQL, array(
																				'email_user' => $this->VARS_HTML['email_user']
																				));			
	}

	/**
	 * UPDATE an active administrator
	 */
	public function activeAdmin(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'update_active_admin.sql';
		$this->result['active_admin']= $this->oBdd->treatDatas($spathSQL, array(
			'id_user' => $this->VARS_HTML['id_user'],
			'active_user' => $this->VARS_HTML['active_user']		
		));
		$this->result['last_insert_id']= $this->oBdd->getLastInsertId();
		error_log('last Id: '. $this->result['last_insert_id'] );
	}
	
	/**
	 * UPDATE a new user
	 *
	 * @param integer $lastInsertId
	 * @param string $uniqueCode
	 * @return void
	 */
	function updateNewUser($lastInsertId, $uniqueCode){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'update_password.sql';
		$this->result['update_new_user'] = $this->oBdd->treatDatas($spathSQL, array(
			'id_user' => $lastInsertId,
																					'password_user' => $uniqueCode																	
																				));	
	}

	/**
	 * UPDATE admin password
	 *
	 * @param integer $idUser
	 * @param string $emailUser
	 * @return void
	 */
	function updateAdminPassword($idUser, $emailUser){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'update_password.sql';
		$this->result['update_new_user'] = $this->oBdd->treatDatas($spathSQL, array(
			'id_user' => $idUser,
			'password_user' => $emailUser																	
		));	
		
	}

	/**
	 * UPDATE all information's of administrator
	 */
	public function updateAdminFull(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'update_single_admin.sql';
		$cryptedPassword = Utils::crypterSSL($this->VARS_HTML['email_user'], $this->VARS_HTML['password_user']);
		$this->result['update_admin']= $this->oBdd->treatDatas($spathSQL, array(
			'id_user' => $this->VARS_HTML['id_user'],
			'id_type' => $this->VARS_HTML['id_type'],
			'name_user' => $this->VARS_HTML['name_user'],
			'email_user' => $this->VARS_HTML['email_user'],
			'password_user' => $cryptedPassword
			
		));
		$this->result['last_insert_id']= $this->oBdd->getLastInsertId();
		error_log('last Id: '. $this->result['last_insert_id'] );
	}
	
	/**
	 * UPDATE an administrator (except password)
	 */
	public function updateAdminSimple(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'update_single_admin_simple.sql';
		$this->result['update_admin']= $this->oBdd->treatDatas($spathSQL, array(
			'id_user' => $this->VARS_HTML['id_user'],
			'id_type' => $this->VARS_HTML['id_type'],
			'name_user' => $this->VARS_HTML['name_user'],
			'email_user' => $this->VARS_HTML['email_user']
		));
		$this->result['last_insert_id']= $this->oBdd->getLastInsertId();
		error_log('last Id: '. $this->result['last_insert_id'] );
	}
	
	
	/**
	 * INSERT default user and user type
	 *
	 * @param string $defaultCode
	 * @param integer $defaultIdType
	 * @return void
	 */
	function insertDefaultUser($defaultCode, $defaultIdType){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'insert_default_user.sql';
		$this->result['default_user'] = $this->oBdd->treatDatas($spathSQL, array(
			'password_user' => $defaultCode,																	
			'id_type' => $defaultIdType				
		));	
		$this->result['last_insert_id'] = $this->oBdd->getLastInsertId();
		error_log('last Id: '. $this->result['last_insert_id'] );
	}
	
	/**
	 * INSERT an administrator
	 */
	public function addAdmin(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'insert_admin.sql';
		$cryptedPassword = Utils::crypterSSL($this->VARS_HTML['email_user'], $this->VARS_HTML['password_user']);
		$this->result['add_admin']= $this->oBdd->treatDatas($spathSQL, array(
																				'id_type' => $this->VARS_HTML['id_type'],
																				'name_user' => $this->VARS_HTML['name_user'],
																				'email_user' => $this->VARS_HTML['email_user'],
																				'password_user' => $cryptedPassword
																		));
		$this->result['last_insert_id']= $this->oBdd->getLastInsertId();
		error_log('last Id: '. $this->result['last_insert_id'] );
	}

	/**
	 * DELETE an administrator
	 */
	public function deleteAdmin(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'delete_admin.sql';
		$this->result['delete_admin']= $this->oBdd->treatDatas($spathSQL, array(
																				'id_user' => $this->VARS_HTML['id_user']		
																			));	
		$this->result['last_insert_id']= $this->oBdd->getLastInsertId();
		error_log('last Id: '. $this->result['last_insert_id'] );
	}
//End of class
}
?>
