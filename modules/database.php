<?php
/**
 * Class database | file database.php
 *
 * In this class, we find all about connection to the data
 *
 * List of classes needed for this class
 * no required class
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */


Class Database {
	/**Global variable - handle */
	private $_hDb;
	
	function __construct($host, $name, $login, $psw)	{
		// Connection to DB : SERVEUR / LOGIN / PASSWORD / NOM_BDD
		$this->_hDb= new PDO('mysql:host='.$host.';dbname='.$name.';charset=utf8', $login, $psw);
	}

	public function __destruct()	{
		$this->_hDb= null;
	}
	
	/**
	 * Method to get last Id
	 * @return int $this->_hDb
	*/
	public function getLastInsertId()	{
		error_log('getLastInsertId DETAILS = '.$this->_hDb->lastInsertId());
		return $this->_hDb->lastInsertId();
	}

	/**Method for select data in database*/
	public function getSelectDatas($spathSQL, $data=array())	{
		// content of SQL file
		$sql= file_get_contents($spathSQL);

		// replace variables @variable from sql by values of the same variables'name
		foreach ($data as $key => $value) {
			$sql = str_replace('@'.$key, $value, $sql);
			error_log("key = " . $key . " | " . "value= " . $value. " | " . "sql = " . $sql);
		}

		error_log("getSelectDatas = " . $sql);

		// Execute the request
		$results_db= $this->_hDb->prepare($sql);
		$results_db->execute();

		if (!$results_db){
			error_log("error = " . $this->_hDb->error);
		}

		$result= [];
		while ($ligne = $results_db->fetch()) {
			$new_ligne= [];
			foreach ($ligne as $key => $value) {
				if (!(is_numeric($key)))	{
					error_log("getSelectDatas DETAILS = " . $key . " => " . $value);
					$new_ligne[$key]= $value;
				}
			}
			$result[]= $new_ligne;
		}

		return $result;
	}
	/**
	 * Method for insert, update, delete in database
	 * @param String $spathSQL
	 * @param array $data=array()
	 * @return array $results_db
	*/
	public function treatDatas($spathSQL, $data=array())	{
		// content of SQL file
		$sql= file_get_contents($spathSQL);
		// $sql= "insert into film (titre_film, date_film, duree_film) values ("@titre_film", "@date_film", @duree_film);"

		/*
		data(
			"titre_film" => $_POST["titre_film"], 
			"date_film" => $_POST["date_film"], 
			"duree_film" => $_POST["duree_film"]
		)
		*/
		// replace variables @variable from sql by values of the same variables'name
		foreach ($data as $key => $value) {
			/*
			$sql= str_replace('@titre_film', "Avengers 3", $sql);
			insert into film (titre_film, date_film, duree_film) values 
				("Avengers 3", "@date_film", @duree_film);

			$sql= str_replace('@date_film', "2018-11-05", $sql);
			insert into film (titre_film, date_film, duree_film) values 
				("Avengers 3", "2018-11-05", @duree_film);

			$sql= str_replace('@duree_film', "123", $sql);
			insert into film (titre_film, date_film, duree_film) values 
				("Avengers 3", "2018-11-05", 123);
			*/
			$sql= str_replace('@'.$key, $value, $sql);
		}

		error_log("treatDatas = " . $sql);

		// Execute the request
		$results_db= $this->_hDb->query($sql);


		if (!$results_db){
			error_log("error = " . $this->_hDb->error);
		}

		return $results_db;
	}
//End of class
}
	
?>
