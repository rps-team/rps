<?php
/**
 * updated 24/08/2019
 */
require_once "client_service.php";
require_once "utils.php";

/**
 * Class client_ajax | file client_ajax.php
 *
 * In this class you will find all ajax post request about to the client page
 * 
 * List of classes needed for this class
 * client_service.php
 * utils.php
 * 
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Client_ajax extends Client_service	{
	

	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * execute main function
	 * 
	 */
		parent::__construct();

		// execute main function
		$this->main();
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * Execute by constructor
	 * Each time an Ajax is post in main function we verify what sort of data are sent
	 * if category mode, update the user category
	 * if save_answers mode, insert rubric data
	 * 		-order data from the post Ajax
	 * 		-build a string with the ordered array
	 * 		-execute the insert with a string parameter
	 * if confirm_survey mode, update date_submitted_survey field with current date in user table
	 */
	public function main()	{
		$this->result['select_user_answers']= [];
        if(isset($this->VARS_HTML['mode']) && $this->VARS_HTML['mode'] == 'category_answers'){
			//update id_type for the logged in ID when user click on "Commencer a repondre" button
			$this->updateUserCategoryPublic();
			//Select all existing answears for the logged in ID
			$this->selectUserAnswersPublic();
			$this->result['nb_form_error'] = 0;
        }
		else if(isset($this->VARS_HTML['mode']) && $this->VARS_HTML['mode']== 'save_answers'){
			//Order datas
			$datasModified = $this->orderPostAjaxDataRubric();
			utils::var_error_log($datasModified);
			$stringIdsQuestion = $this->buildStringAllIdQuestion($datasModified);
			//delete datas from data
			error_log($stringIdsQuestion);
			$this->deleteUserAnswersPublic($stringIdsQuestion);
			//Build a string of values for the request
			$stringDataRubric = $this->buildStringValues($datasModified);
			error_log($stringDataRubric);
			//execute the insert of values
			$this->insertUserAnswersPublic($stringDataRubric);	
			$this->result['nb_form_error'] = 0;
		}
		else if(isset($this->VARS_HTML['mode']) && $this->VARS_HTML['mode'] == 'confirm_survey'){
			if(!isset($_SESSION['date_submitted_survey']) || (isset($_SESSION['date_submitted_survey']) && $_SESSION['date_submitted_survey'] == '')){
				$this->confirmSurveyPublic();
				$this->result['nb_form_error'] = 10;
			}
			else
				$this->result['nb_form_error'] = 11;
		}
	}

	/**
	 * Called in main function
	 * Treat datas from the POST ajax about rubric to save
	 * Remove unnecessary datas,
	 * return an indexed and ordered array of data ready to be parse by the next function 
	 * 
	 * @return array $datasModified
	 * 
	 */
	private function orderPostAjaxDataRubric(){
		$postAjaxRubric = $this->VARS_HTML;
		$suggestionPathern = '/^radioSuggestion_/';
		$commentPathern = '/^comment_/';
		$idQuestion = 0;
		$oldIdQuestion = 0;
		$count = 0;
		$datasModified=[];
		//clean the postAjaxRubric array from unnecessary datas
		unset($postAjaxRubric['bJSON'],$postAjaxRubric['page'], $postAjaxRubric['mode']);
		utils::var_error_log($postAjaxRubric);
		
		//Parse and order datas question by question
		foreach($postAjaxRubric as $key => $value){
			if($oldIdQuestion != 0){
				$idQuestion = (int) filter_var($key, FILTER_SANITIZE_NUMBER_INT);
			}
			$isRadioSuggestion= preg_match($suggestionPathern, $key);
			$isComment = preg_match($commentPathern, $key);
			
			if($idQuestion == $oldIdQuestion){
				if($isRadioSuggestion){
					$datasModified[$count]['id_suggestion'] = $value;
				}
				if($isComment){
					$datasModified[$count]['comment'] = $value;
				}		
			}
			else{
				//increment the index of the array to stack new question datas together
				$count ++;
				if($isRadioSuggestion){
					$datasModified[$count]['id_suggestion'] = $value;
				}
				if($isComment){
					$datasModified[$count]['comment'] = $value;
				}
			}
			$oldIdQuestion = (int) filter_var($key, FILTER_SANITIZE_NUMBER_INT);
			$datasModified[$count]['id_question'] = (int) filter_var($key, FILTER_SANITIZE_NUMBER_INT);
		}
		return $datasModified;
	}
	
	/**
	 * Called in main function
	 * transform an array of data rubric into a single var string type
	 *
	 * @param array $datasModified
	 * @return string $stringIdsQuestion
	 *
	 */
	private function buildStringValues($datasModified){
		$stringDataRubric ='';
		for($i=0; $i < sizeof($datasModified); $i++){
			//verify if the comment not exist and if yes create an empty comment to avoid undefine error when excute request
			if(!isset($datasModified[$i]['comment'])){
				$datasModified[$i]['comment']= NULL;
			}
			//verify if id_suggestion exist and if not create a id_suggestion to Null to avoid undefine error when execute request
			if ($datasModified[$i]['id_suggestion'] == "")	{
				$datasModified[$i]['id_suggestion']= "null";
			}
			//construct the string to return
			$stringDataRubric .= '('.$_SESSION['id_user'].','.$datasModified[$i]['id_question'].','.$datasModified[$i]['id_suggestion'].',"'.$datasModified[$i]['comment'].'"),';	
		}
		//remove last character of the string ','
		$stringDataRubric = substr_replace($stringDataRubric , "", -1);
		return $stringDataRubric;
	}

	private function buildStringAllIdQuestion($datasModified){
		$stringIdsQuestion = '';
		for($i=0; $i < sizeof($datasModified); $i++){
			$stringIdsQuestion .= $datasModified[$i]['id_question'].',';
		}
		$stringIdsQuestion = substr_replace($stringIdsQuestion , "", -1);
		return $stringIdsQuestion;
	}

//End of class
}

?>
