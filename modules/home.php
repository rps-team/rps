<?php
/**
 * updated 24/08/2019
 */
require_once "admin_service.php";

/**
 * Class Home | file home.php
 *
 * In this class, we find all about home page
 *
 * List of classes needed for this class
 * admin_service.php
 *
 * 
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Home extends Admin_service {

	
	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * init variables result
	 *
	 * execute main function
	 * 
	 */
		parent::__construct();
		// execute main function
		$this->main();
	}
	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * Do nothing
	 *
	 * @return void
	 */
	public function main()	{
	
		//nothing to do
		//render view only
	}
//End of class
}