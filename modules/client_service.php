<?php
/**
 * updated 24/08/2019
 */
require_once 'initialize.php';
require_once 'utils.php';

/**
 * Class Client_service | file client_service.php
 *
 * In this class, we find all functions about request 
 * for index page and client page
 * index class, 
 * client class, 
 * client_ajax class
 * all extends this class
 *
 * List of classes needed for this class
 * initialize.php
 * utils.php
 *
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Client_service extends Initialize	{
	/**
	 * public $result is used to store all data needed for HTML Templates and ajax_json: 
	 * index.html, 
	 * client_ajax.html(json), 
	 * client.html,
	 * @var array $result
	 * @var object $oBdd
	 */
	public $result;
	private $oBdd;
	
	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * init variables result
	 *
	 * execute main function
	 * 
	 * charge the title for this class to send to header_admin.html using result 
	 */
		$this->oBdd= parent::__construct();
		// init variables result
		$this->result= [];
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * SELECT active survey title
	 */
	 public function selectActiveSurveyTitle()	{

		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_active_survey_title_public.sql';
		$this->result['active_survey_title']= $this->oBdd->getSelectDatas($spathSQL, array());
	}


	/**
	 * SELECT category form Db
	 */
	public function selectCategory()	{

		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_category_list_public.sql';
		$this->result['category_list']= $this->oBdd->getSelectDatas($spathSQL, array());

	}

	/**
	 * SELECT all the active survey, with themes, rubrics, questions and suggestions
	 */
	public function selectSurveyPublic()	{

		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_survey_public.sql';
		$this->result['data_survey']= $this->oBdd->getSelectDatas($spathSQL, array(
																					'id_survey' => $_SESSION['id_survey']
		));

	}

	/**
	 * SELECT a single user
	 */
	public function selectSingleUser()	{

		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_single_user.sql';
		$this->result['single_user']= $this->oBdd->getSelectDatas($spathSQL, array('password_user' => $this->VARS_HTML['password_user']));
	}
	
	/**
	 * SELECT a single user data
	 */
	public function selectUserAnswersPublic()	{
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'select_user_answers_public.sql';
		$this->result['select_user_answers']= $this->oBdd->getSelectDatas($spathSQL, array(
																					'id_user' => $_SESSION['id_user']
																			));
	}

	/**
	 * UPDATE a single user category
	 */
	public function updateUserCategoryPublic(){

		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'update_user_category_public.sql';
		$this->result['update_category']= $this->oBdd->treatDatas($spathSQL, array(
			'id_user' => $_SESSION['id_user'],
			'id_type' => $this->VARS_HTML['id_type']
		));
	}
	
	/**
	 * UPDATE for a connected user the submit survey date field
	 *
	 * @return void
	 */
	public function confirmSurveyPublic(){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'update_user_confirm_survey_public.sql';
		$this->result['delete_answers']= $this->oBdd->treatDatas($spathSQL, array(
			'id_user' => $_SESSION['id_user']
		));
	}

	/**
	 * INSERT all data for one rubric
	 *
	 * @param string $stringDataRubric
	 * @return void
	 */
	public function insertUserAnswersPublic($stringDataRubric){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'insert_user_answers_public.sql';
		$this->result['insert_answers']= $this->oBdd->treatDatas($spathSQL, array(
																				'datasRubric' => $stringDataRubric
																			));
	}

	/**
	 * DELETE answers of multiple questions 
	 *
	 * @param string $stringIdsQuestion
	 * @return void
	 */
	public function deleteUserAnswersPublic($stringIdsQuestion){
		$spathSQL= $this->GLOBALS_INI['PATH_HOME'] . $this->GLOBALS_INI['PATH_SQL'] . 'delete_user_answers_public.sql';
		$this->result['delete_answers']= $this->oBdd->treatDatas($spathSQL, array(
																				'id_user' => $_SESSION['id_user'],
																				'stringIdsQuestion' => $stringIdsQuestion
																			));
	}
	
//End of class
}

?>
