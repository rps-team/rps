<?php
/**
 * updated 24/08/2019
 */
require_once "admin_service.php";
/**
 * Class Administer | file administer.php
 *
 * In this class, we find all functions about send user code and refresh administer page 
 *
 * List of classes needed for this class
 * admin_service.php
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Administer extends Admin_service	{
	
	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * execute main function
	 * 
	 */
		parent::__construct();

		// execute main function
		$this->main();
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * 
	 * This the main function start at loading when constructor is called
	 * Here is where the main code is. Here I can Access to :
	 * Public VARS_HTML from initialize.php
	 * Public result from admin_service.php
	 * ALL methods of admin_service.php
	 * Create new user
	 * Send an email with the code user when ENVOYER button is clicked
	 * 
	 */
	public function main()	{
		//SELECT administrator list fill result['admin_list'] in admin_service
		$this->adminList();

		//Execute only if ENVOYER button is clicked (Envoyer un code)
		if(isset($this->VARS_HTML['submit_mail'])){
			//execute this code if current_time is different and mail_to different 
			if($_SESSION['current_time'] != $this->VARS_HTML['current_time'] && !empty($this->VARS_HTML['mail_to'])){
				$defaultCode = "12345678";
				$defaultIdType = 3;
				$this->insertDefaultUser($defaultCode, $defaultIdType);
				$lastInsertId = $this->result['last_insert_id'];
				$this->selectLastUser($lastInsertId);
				$dayOfLastInsertUser = $this->result['last_user'][0]['day'];
				$uniqueCode= $this->createUniqueCode($lastInsertId , $dayOfLastInsertUser);
				$this->updateNewUser($lastInsertId, $uniqueCode);
				$this->mailTo($uniqueCode);
				$_SESSION['current_time'] = $this->VARS_HTML['current_time'];
			}
			else{
				header('Location:administer');
			}
		}
	}


	/**
	 * 
	 * Prepare and send an email with the code to a given email (new user)
	 *
	 * @param string $uniqueCode
	 * @return void
	 */
	private function mailTo($uniqueCode){
		$mail = $this->VARS_HTML['mail_to'];
		$subject = 'Votre code d\'accès:'. $_SESSION['wording_survey'];
		$message = '';
		$message .= '<p>Voici votre code d\'accès :  <strong>'.$uniqueCode.'</strong></br></p>';
		$message .= '<p>Il vous permettra de répondre au questionnaire '.$_SESSION['wording_survey'].'</br></p>';
		$message .= '<p>Très cordialement</br></p>';
		$message .= '<p>Raymonde RICOURT </br></p>';
		$message .= '<p>Secrétaire au CHSCT de l\'Afpa </br></p>';

		$isSent= mail($mail,$subject,$message);

		if($isSent){
			echo 'email envoyé </br>';
			echo 'To:'.$mail.'- Subject:'.$subject.'- Message:'.$message.'</br>';
			var_dump($isSent);
		}
	}

	/**
	* Create a code
	*
	* Take lastInsertId as parameter
	* create an unique code based on user id
	* return an unique code
	* Code pattern  AA BB CC DD 
	* 		-AA = 2 first digits of the hexadecimal from the integer lastUserId
	* 		-BB = day of the insert of the last user id with 0 added behind if single digit
	* 		-CC = a random number between 10 and 99
	* 		-DD = 2 last digits of the hexadecimal from the integer lastUserId
	* 
	* @param integer $lastInsertId
	* @param string $dayOfLastInsertUser (from sql request)
	* @return string $uniqueCode
	* 
	*/
	private function createUniqueCode($lastInsertId, $dayOfLastInsertUser){
		$uniqueCode = "12345678"; //default unique code
		$randomCode = rand(10,99); // random number for  CC
		if(strlen($dayOfLastInsertUser) != 2 ){
			$dayOfLastInsertUser .= '0' ;
		}
		$lastInsertId = dechex($lastInsertId); // transform to Hexadecimal
		$lastUserIdLength = strlen($lastInsertId);
		//fix the hexadecimal to 4 digits equivalent to AADD (65535 possibilities)
		switch ($lastUserIdLength)
		{
			case 1 :
				$lastInsertId = '000' .$lastInsertId; 
				echo 'Trois zeros à ajouter</br>' ;
			break;

			case 2 :
				$lastInsertId = '00' .$lastInsertId;
				echo 'Deux zéros à ajouter</br>';
			break; 

			case 3 :
			$lastInsertId = '0' .$lastInsertId;
				echo 'Un zéro à ajouter</br>';
			break;

			default:
				echo 'Pas de zéro à ajouter</br>';	
			break; 		
		}
		//slice in 2 parts the hexadecimal return an array of the 2 parts array [0] = AA , array [0] = DD 
		$hexaIdSplited = str_split($lastInsertId,2);
		//assemble the unique code using the pattern  AA BB CC DD 
		$uniqueCode = $hexaIdSplited[0]. $dayOfLastInsertUser . $randomCode. $hexaIdSplited[1];
		return $uniqueCode;
	}
//End of class
}

?>
