<?php
/**
 * updated 24/08/2019
 */
require_once "admin_service.php";

/**
 * Class Statistic | file statistic.php
 *
 * In this class, we find all about statistic page
 *
 * List of classes needed for this class
 * admin_service.php
 *
 * 
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Statistic extends Admin_service	{
	
	
	public function __construct()	{
		// SESSION
		// session_start();
		// Call Parent Constructor
		parent::__construct();

		// execute main function
		$this->main();
	}

	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * Execute when constructor is called 
	 *
	 * @return void
	 */
	public function main()	{
		/*
		* TO DO-1 // select of all question on a given survey
		* TO DO-2 // for each question of a given survey select count suggestion 1, 2, 3, 4 
		* TO DO-3 // select count users where user_type = user (total number of code)
		* TO DO-4 // select count users where date_submitted_survey exist (total submitted survey)
		* TO DO-5 // use in template js chart generic method already made in statistic.js
		* use foreach of TO DO-2  build onload js method contain array of each question  
		*/
	}
//End of class
}

?>
