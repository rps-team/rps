<?php
/**
 * updated 24/08/2019
 */
require_once "admin_service.php";
require_once "utils.php";

/**
 * Class Admin | file admin.php
 *
 * In this class, we manage all about admin page (for ajax see admin_ajax.php)
 *
 * List of classes needed for this class
 * admin_service.php
 * utils.php
 *
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Admin	extends Admin_service{
	
	public function __construct()	{
		// SESSION
		// session_start();
		// Call Parent Constructor
		parent::__construct();

		// execute main function
		$this->main();
	}

	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * 
	 * This is the main function call in the constructor 
	 * 
	 * Check if recaptcha is valid
	 * Check if datas are well filled by the user
	 * Verify if nbState = '0' 
	 * 
	 * if true :
	 * 		Verify is the request return content email and password
	 * 		Compare given user data with bdd data
	 * 		if good start session send to the page Home
	 * if false: 
	 *		 send message to the view admin.html with result['message_error'] and result['code_error']
	 * * */
	public function main()	{
		//session destroy at start of load
		session_destroy();
		error_log('THE SESSION HAS BEEN DESTOYER');
		$this->result['message_error'] = '';
		$cryptedPassFromAdmin = '';
		$cryptedPassFromBdd = '';
		//if submit is pressed do the code
		if(isset($this->VARS_HTML['submitpost'])){
			//verify the if reCaptcha is valid first
			$isReCaptchaValid = Utils::reCaptcha($this->VARS_HTML);
			if($isReCaptchaValid){
				//verify if email and passord exist
				if(isset($this->VARS_HTML['email_user']) && isset($this->VARS_HTML['password_user'])){
					$nbState= 0;
					//control all datas from inputs fileds and return a codestate array
					$codeStateArray = $this->checkData();
					//parse the array and try to find an nbState != 0 else nbState stay  0
					for($i =0; $i<count($codeStateArray); $i++) 
					{
						error_log('CODE STATE N°'.$i. ' = '.$codeStateArray[$i]);
						if($codeStateArray[$i] != 0)
						{
							$nbState = $codeStateArray[$i];
							error_log('Number of STATE: '.$nbState);
						}
					}
					// If all field are well filled and recaptcha ok  nbState should be = 0
					if($nbState == 0){
						$nbState = 3;
						//crypt provided password
						$cryptedPassFromAdmin = Utils::crypterssl($this->VARS_HTML['email_user'],$this->VARS_HTML['password_user']);
						//request user with provided password
						$this->selectSingleAdmin();
						//if exist compare bdd password with provided password
						if(isset( $this->result['select_admin'][0]['password_user'])&& isset($this->result['select_admin'][0]['email_user'])){
							$nbState = 0;
							$cryptedPassFromBdd = $this->result['select_admin'][0]['password_user'];
							error_log('cryptedPassFromBdd :'.$cryptedPassFromBdd);
							$emailFromBdd = $this->result['select_admin'][0]['email_user'];
							$isPasswordExist = $cryptedPassFromAdmin == $cryptedPassFromBdd;
							$isEmailExist = $cryptedPassFromAdmin == $cryptedPassFromBdd;
							// if admin is confirmed start session fill $_SESSION with needed datas
							if($isPasswordExist && $isEmailExist){
								$this->startNewSession();
							}
						}
					}
					$this->setMessageAndCodeError($nbState);
				}
			}
			else{
				$nbState = 7;
				$this->setMessageAndCodeError($nbState);
			}
		}	
	}



	/**
	 * 
	 * Called in main function
	 * it start a new session when called
	 * @return void
	 * 
	 * */
	 private function startNewSession()	{
		session_start();
		error_log('THE SESSION HAS BEEN STARTED');
		$_SESSION['id_user']=$this->result['select_admin'][0]['id_user'];
		$_SESSION['name_user']=$this->result['select_admin'][0]['name_user'];
		$_SESSION['user_type']=$this->result['select_admin'][0]['user_type'];
		$_SESSION['right_type']=$this->result['select_admin'][0]['right_type'];
		$_SESSION['current_time']= time();
		//select active survey
		$this->selectActiveSurveyTitle();
		$_SESSION['id_survey'] = $this->result['active_survey_title'][0]['id_survey'];
		$_SESSION['wording_survey'] = $this->result['active_survey_title'][0]['wording_survey'];
		//re direct to the administrator home page
		header('Location:home');
	}


	/**
	 * 
	 * Called in main function
	 * It check data: 
	 * if password and email are valid and well filled
	 * @return array $codeStateArray
	 * 
	 * */
	private function checkData()	{
		$codeStateArray =[];
		$email_user = '';
		$password_user = '';
		$email_user = $this->VARS_HTML['email_user'];
		$password_user = $this->VARS_HTML['password_user'];
		$codeStateArray[0]= $this->controlPassword($password_user, $password_user);
		$codeStateArray[1]= $this->controlEmail($email_user);
	
		return $codeStateArray;
	}

	
	/**
	* set message and error code
	*
	* @param integer $nbState
	* @return void
	*/
	private function setMessageAndCodeError($nbState){
		if($nbState == 2){
			$this->result['message_error'] = 'Champ email vide';
			$this->result['code_error'] = 'a';
		}
		else if($nbState == 4){
			$this->result['message_error'] = 'Champ password vide';
			$this->result['code_error'] = 'b';
		}
		else if($nbState == 3 || $nbState == 6){
			$this->result['message_error'] = 'Email ou mot de passe invalide';
			$this->result['code_error'] = 'c';
		}
		else if($nbState == 7){
			$this->result['message_error'] = 'reCaptcha invalide';
		}			
	}

	/**
	* Called in checkdata function
	* Verify if email is filled and valid
	* return an state number
	*
	* @param string $emailToCompare
	* @return integer $nbState
	*/
	private function controlEmail($emailToCompare){
		error_log('controlEmail function started');
		$nbState ='';
		$isEmailValid = false;
		$isEmailEmpty = true;
		$isEmailEmpty = !(isset($emailToCompare)) || $emailToCompare == '';
		if(!$isEmailEmpty){
			$isEmailValid = filter_var($emailToCompare, FILTER_VALIDATE_EMAIL);
			if($isEmailValid){
				$nbState = 0;
			}
			else{
				$nbState = 3;
			}
		}
		else{
			$nbState = 2;
		}
		error_log('isEmailValid:'.$nbState);
		return $nbState;
	}
	
	/**
	* Called in checkdata function
	* Verify if the password is filled and valid
	* return an state number
	*
	* @param string $passwordToCompare
	* @param string $confirmToCompare
	* @return void
	*/
	private function controlPassword($passwordToCompare, $confirmToCompare){
		error_log('controlPassword function started');
		$nbState ='';
		$isPasswordEmpty = !(isset($passwordToCompare)) || $passwordToCompare == '';
		$isConfirmEmpty =!(isset($confirmToCompare)) || $confirmToCompare == '';
		if(!$isPasswordEmpty || !$isConfirmEmpty)
		{
			$isEqual = $passwordToCompare == $confirmToCompare;
			if($isEqual)
			{
				$pathern = '/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!\.*$@%_])([-+!\.*$@%_\w]{8,15})$/';
				$isPasswordValid = preg_match($pathern, $confirmToCompare);
				if($isPasswordValid)
				{
					$nbState ='0';
					
				}
				else
				{
					$nbState ='6';
				}
			}
			else
			{
				$nbState ='5';
			}
		}
		else
		{
			$nbState ='4';	
		} 
		error_log('isPasswordValid:'.$nbState);
		return $nbState;
	}
//End of class
}
?>