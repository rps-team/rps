<?php
/**
 * updated 24/08/2019
 */
require_once 'client_service.php';
/**
 * Class Index | file index.php
 *
 * In this class, we find all about index page
 *
 * List of classes needed for this class
 * client_service.php
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Index extends Client_service	{

	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * execute main function
	 * 
	 */
		parent::__construct();

		// execute main function
		$this->main();
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * 
	 * Execute when constructor is called
	 * Verify if the password exist in Db
	 * Destroy eventual SESSION  
	 * Control the type of given user code
	 * if not good location to the same page with message invalid password
	 * if good  location to page client and open session of the user
	 * 
	 */
	function main()	{
		session_destroy();
		error_log('THE SESSION HAS BEEN DESTOYER');
		$this->selectActiveSurveyTitle();
		$this->result['bCodeOK']= '';

		//Verify if a password was submit
		if((isset($this->VARS_HTML['password_user'])) && $this->VARS_HTML['password_user'] != ''){	
		//verify password user content from request is a valid type
		$isCodeValidFromRequest = $this->controlCodeFromRequest();
		//verify if the user exist
		$this->selectSingleUser();
		//verify if the code exist in BDD and the date of the user code is valid too
		$isUserValidFromBdd = $this->checkUserFromBdd();
		//if both condition are true open session and redirect to client
			if($isUserValidFromBdd && $isCodeValidFromRequest){
				session_start();
				error_log('THE SESSION HAS BEEN STARTED');
				$_SESSION['id_user'] = $this->result['single_user'][0]['id_user'];
				$_SESSION['id_type'] = $this->result['single_user'][0]['id_type'];
				$_SESSION['user_type'] = $this->result['single_user'][0]['user_type'];
				$_SESSION['right_type'] = $this->result['single_user'][0]['right_type'];
				$_SESSION['date_submitted_survey'] = $this->result['single_user'][0]['date_submited_survey'];
				$_SESSION['id_survey'] = $this->result['active_survey_title'][0]['id_survey'];
				$_SESSION['wording_survey'] = $this->result['active_survey_title'][0]['wording_survey'];
				error_log('ID SURVEY SESSION WHEN SESSION START: '.$_SESSION['id_survey']);
				// $this->result['bCodeOK'] =  0;
				// le code est bon on redirige vers client
				header('Location: client');
			}
			else{
				// else, stay on index and send an error code = 1  
				$this->result['bCodeOK'] = 1;
			}
			error_log('bCodeOK = ' . $this->result['bCodeOK']);
		}
	}

	/**
	 * Verify is user exist in Db
	 *
	 * @return boolean $isCodeValidFromBdd && $isDateValid
	 */
	function checkUserFromBdd(){
		$isDateValid = true;
		$isCodeValidFromBdd = isset($this->result['single_user'][0]['password_user'])&& $this->result['single_user'][0]['password_user'] !=''&& $this->result['single_user'][0]['password_user'] == $this->VARS_HTML['password_user'];
		if(isset($this->result['single_user'][0]['date_submited_survey'])&& $this->result['single_user'][0]['date_submited_survey'] !=''){
			$currentDate = new DateTime('today');
			$submittedSurveyDate = new DateTime($this->result['single_user'][0]['date_submited_survey']);
			$submittedSurveyDate->modify('+2 day');
			error_log('CURRENT DATE IS: '.$currentDate->format('Y-m-d'));
			error_log('SUBMITTED DATE +2 DAYS IS: '.$submittedSurveyDate->format('Y-m-d'));
			if($currentDate < $submittedSurveyDate){
				$isDateValid = true;
			}
			else{
				$isDateValid =false;

			}
		}
		return $isCodeValidFromBdd && $isDateValid;
	}

	/**
	 * Verify if the given code is valid pattern 
	 * number and letters only
	 * size 8 digit
	 *
	 * @return boolean isCodeValidFromRequest
	 */
	function controlCodeFromRequest(){
		$isCodeValidFromRequest = false;
		if((isset($this->VARS_HTML['password_user'])) && $this->VARS_HTML['password_user'] != ''){		
			$passwordUser = $this->VARS_HTML['password_user'];
			$passwordUser = strtolower($passwordUser); 
			$pattern = '/^[a-z0-9]+$/';
			$isPatternValid = preg_match($pattern, $passwordUser);

			if(strlen($passwordUser) == 8 && $isPatternValid = true){
				$isCodeValidFromRequest = true;
				error_log('CODE PATTERN OK');
			}	
		}
		return $isCodeValidFromRequest;
	}
//End of class
}
?>
