<?php
/**
 * updated 24/08/2019
 */
require_once "admin_service.php";

/**
 * Class Administer_ajax | file administer_ajax.php
 *
 * In this class, we find all about administer page
 * administrator CRUD with AJAX from administer_ajax.js
 *
 * List of classes needed for this class
 * admin_service.php
 * utils.php
 *
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Administer_ajax extends Admin_service {

	
	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * init variables result
	 *
	 * execute main function
	 * 
	 */
		parent::__construct();
		// execute main function
		$this->main();
	}
	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}


	/**
	 * When called by post ajax (administer_ajax.js),
	 * Control if the mode exist with controlMode function return the mode
	 * catch state of each mode to see witch mode is active (true) 
	 * if active mode : change active_user bu request activeAdmin()
	 * if delete mode : delete the admin by request deleteAdmin()
	 * if add or edit mode:  
	 * verify data with checkData()
	 * parse the arrayOfErrors and fill nb_form_error with the last error
	 * if nb_form_error == 0
	 * 			if edit mode : edit the admin by request using: 
	 *  		updateAdminSimple() or updateAdminfull() 
	 * 			if add mode : add the admin by request addAdmin()
	 */
	public function main()	
	{	$this->result['last_insert_id'] = '';
		$this->result['nb_form_error'] = 0;
		error_log('function MAIN started');
		$mode = $this->controlMode();
		error_log('MODE:'. $mode);

		if ($mode == 'active' && isset($this->VARS_HTML['id_user']))
		{
			error_log('ACTIVE Mode selected');
			$this->activeAdmin();
			$this->result['nb_form_error'] = 0;
		}
		else if ($mode == 'delete' && isset($this->VARS_HTML['id_user']))	
		{
			error_log('DELETING Mode selected');
			$this->deleteAdmin();
			$this->result['nb_form_error'] = 0;
		}
		else if ($mode == 'add' || $mode == 'edit')
		{		
			$arrayOfErrors = $this->checkData();
			for($i =0; $i<count($arrayOfErrors); $i++) 
			{
				error_log('error N°'.$i. ' = '.$arrayOfErrors[$i]);
				if($arrayOfErrors[$i] != 0)
				{
					$this->result['nb_form_error'] = $arrayOfErrors[$i];
					error_log('nb_form_error: '.$this->result['nb_form_error']);
				}
			}
			
			error_log('nb_form_error value:'. $this->result['nb_form_error']);
			
			if($mode == 'edit' && isset($this->VARS_HTML['id_user']))	
			{	
				error_log('EDITING Mode selected');
				if ($this->result["nb_form_error"] == 0)	
				{
					//A duplicate email could exist
					//if admin provide existing email when changing email
					//To Do  --> if email CHANGE use checkIfEmailExist() 
					//return $this->result["nb_form_error"] == 7 to the view
					if (($this->VARS_HTML['password_user'] == "") && ($this->VARS_HTML['password_confirm_user'] == ""))	{
						$this->updateAdminSimple();
					}  else  {
						$this->updateAdminFull();
					}
				}
			}
				
			else if($mode == 'add')	
			{
				error_log('ADDING Mode selected');
				if ($this->result["nb_form_error"] == 0)	
				{
					$isEmailNotExist = $this->checkIfEmailExist();
					if($isEmailNotExist){
						$this->addAdmin();
					}
					else{
						$this->result["nb_form_error"] = 7;
					}
							
				}
			}
		}
	}

	/**
	 * Called by main function,
	 * initialize vars with VARS_HTML content
	 * if edit mode and :
	 * 		if password field are both empty then don't do controlPassword()
	 * 		else do all controls
	 * else if add mode :
	 * 		do all controls
	 * put each result error in arrayOfErrors,
	 * 
	 * @return array $arrayOfErrors
	 * 
	 */
	private function checkData()	
	{
		$arrayOfErrors =[];
		$mode ='';
		$name_user = '';
		$email_user = '';
		$password_user = '';
		$password_confirm_user = '';
		$mode = $this->VARS_HTML['mode'];
		$name_user = $this->VARS_HTML['name_user'];
		$email_user = $this->VARS_HTML['email_user'];
		$password_user = $this->VARS_HTML['password_user'];
		$password_confirm_user = $this->VARS_HTML['password_confirm_user'];
		if($mode == 'edit')
		{	
			if(!(empty($password_user) && empty($password_confirm_user))){
				$arrayOfErrors[0]= $this->controlPassword($password_user, $password_confirm_user);
				$arrayOfErrors[1]= $this->controlEmail($email_user);
				$arrayOfErrors[2]= $this->controlName($name_user);
			}
			else{
				$arrayOfErrors[0]= $this->controlEmail($email_user);
				$arrayOfErrors[1]= $this->controlName($name_user);
			}
			
		}
		else if($mode == 'add')
		{
			$arrayOfErrors[0]= $this->controlPassword($password_user, $password_confirm_user);
			$arrayOfErrors[1]= $this->controlEmail($email_user);
			$arrayOfErrors[2]= $this->controlName($name_user);
			
			
		}
		return $arrayOfErrors;
	}

	/**
	 * Called by main function,
	 * Control if mode variable is POST by ajax
	 * return the mode to the main function (string)
	 * 	 
	 * @return string $mode
	 * 
	 */
	private function controlMode()
	{
		if(isset($this->VARS_HTML['mode'])|| !($this->VARS_HTML['mode'] == ''))
		{
			$mode= $this->VARS_HTML['mode'];
		}
		return $mode;
	}
	
	/**
	 * Called by checkData function,
	 * verify if name_user has been provide by the user,
	 * Verify:
	 * 0 VALID
	 * 1 EMPTY FIELD
	 * return nb_error state to checkData
	 * 
	 * @param string $nameToVerify
	 * @return integer $nb_error
	 * 
	 */
	private function controlName($nameToVerify)
	{
		error_log('controlName function started');
		$nb_error ='';
		$isNameEmpty = true;
		$isNameEmpty = !(isset($nameToVerify)) || $nameToVerify == '';
		if(!$isNameEmpty){
			$nb_error = 0;
		}
		else{
			$nb_error = 1;
		}
		error_log('isNameValid:'.$nb_error);
		return $nb_error;
	}
	
	/**
	 * Called by checkData function,
	 * verify if email_user has been provide by the user,
	 * verify if email_user is a valid type using preg_match,
	 * Verify:
	 * 0 VALID
	 * 2 EMPTY FIELD
	 * 3 INVALID TYPE 
	 * return nb_error state to checkData
	 * 
	 * @param string $emailToCompare
	 * @return integer $nb_error
	 * 
	 */
	private function controlEmail($emailToCompare)
	{
		error_log('controlEmail function started');
		$nb_error ='';
		$isEmailValid = false;
		$isEmailEmpty = true;
		$isEmailEmpty = !(isset($emailToCompare)) || $emailToCompare == '';
		if(!$isEmailEmpty){
			$pattern = '/^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$/';
			$isEmailValid = preg_match($pattern, $emailToCompare);

			if($isEmailValid){
					$nb_error = 0;
			}
			else{
				$nb_error = 3;
			}
		}
		else{
			$nb_error = 2;
		}
		error_log('isEmailValid:'.$nb_error);
		return $nb_error;
	}

	/**
	 * Called by checkData function,
	 * verify if password_user and password_confirm_user have been provide by the user,
	 * verify if password_user and password_confirm_user ARE EQUALS
	 * verify if password_user is valid type using preg_match 
	 * Verify:
	 * 0 VALID
	 * 4 EMPTY FIELD
	 * 5 NOT EQUAL
	 * 6 INVALID TYPE 
	 * return nb_error state to checkData
	 * 
	 * @param string $passwordToCompare
	 * @param string $confirmToCompare
	 * @return integer $nb_error
	 * 
	 */
	private function controlPassword($passwordToCompare, $confirmToCompare)
	{
		error_log('controlPassword function started');
		$nb_error ='';
		$isPasswordEmpty = !(isset($passwordToCompare)) || $passwordToCompare == '';
		$isConfirmEmpty =!(isset($confirmToCompare)) || $confirmToCompare == '';
		if(!$isPasswordEmpty || !$isConfirmEmpty)
		{
			$isEqual = $passwordToCompare == $confirmToCompare;
			if($isEqual)
			{
				$pattern = '/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!\.*$@%_])([-+!\.*$@%_\w]{8,15})$/';
				$isPasswordValid = preg_match($pattern, $confirmToCompare);
				if($isPasswordValid)
				{
					$nb_error ='0';
					
				}
				else
				{
					$nb_error ='6';
				}
			}
			else
			{
				$nb_error ='5';
			}
		}
		else
		{
			$nb_error ='4';	
		} 
		error_log('isPasswordValid:'.$nb_error);
		return $nb_error;
	}
	/**
	 * Verify if the given email exist in database
	 *
	 * @return boolean $isEmailNotExist
	 */
	private function checkIfEmailExist(){
		$isEmailNotExist = false;
		$this->selectExistingEmail();
		$isEmailNotExist = !isset($this->result['select_existing_email'][0]['email_user']) 
		|| (isset($this->result['select_existing_email'][0]['email_user']) && $this->result['select_existing_email'][0]['email_user'] == "");
		error_log('IS EMAIL NOT EXIST : ' . $isEmailNotExist);
		return $isEmailNotExist;
	}

//End of class
}
?>
