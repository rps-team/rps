<?php
/**
 * updated 24/08/2019
 */
require_once "admin_service.php";

/**
 * Class Survey | file survey.php
 *
 * In this class, we find all about survey page
 *
 * List of classes needed for this class
 * admin_service.php
 *
 * 
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Survey extends Admin_service	{
	

	public function __construct()	{
		// SESSION
		// session_start();
		// Call Parent Constructor
		parent::__construct();

		// execute main function
		$this->main();
	}

	public function __destruct()	{
		parent::__destruct();
	}


	/**
	 * Execute when constructor is called
	 *
	 * @return void
	 */
	public function main()	{


	}
//End of class
}

?>