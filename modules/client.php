<?php
/**
 * updated 24/08/2019
 */
require_once "client_service.php";

/**
 * Class client | file client.php
 *
 * In this class, we find all about client page (except ajax method)
 *
 * List of classes needed for this class
 * client_service.php
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Client extends Client_service	{
	

	public function __construct()	{
	/**
	 * Call the parent constructor
	 *
	 * execute main function
	 * 
	 */
		parent::__construct();

		// execute main function
		$this->main();
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * 
	 * Execute when constructor is called
	 */
	function main()	{
		//select the active survey title
		$this->selectActiveSurveyTitle();
		
		//select the connected user category and fill result['category_list']
		$this->selectCategory();
		
		//Fill result['id_type'] with id_type from user SESSION for the view
		$this->result['id_type'] = $_SESSION['id_type'];

		//select the active survey data and fill result['data_survey'] for the view
		$this->selectSurveyPublic();
	}

//End of class
}

?>
