<?php
/**
 * updated 22/08/2019
 */
require_once "admin_service.php";
require_once "utils.php";

/**
 * Class Admin_ajax | file admin_ajax.php
 *
 * In this class, we manage all about administrator connection using ajax  
 * 
 * 
 * List of classes needed for this class
 * admin_service.php
 * utils.php
 *
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class Admin_ajax extends Admin_service{
	
	public function __construct()	{
		// SESSION
		// session_start();
		// Call Parent Constructor
		parent::__construct();

		// execute main function
		$this->main();
	}

	public function __destruct()	{
		parent::__destruct();
	}

	/**
	 * 
	 * Called when "Envoyer un code de ré initialisation" btn is clicked
	 * verify if the mail is valid
	 * send an reinit email for to a given mail
	 * --------------------------
	 * todo  finish crypt id - see why decrypt not working on forgot page
	 * --------------------------
	 */
	public function main()	{	
		$mode = $this->VARS_HTML['mode'];
		$emailUserForgot = $this->VARS_HTML['email_user_forgot'];
		//verify if ajax post exist,  if yes control email
		if(isset($mode) && $mode == 'initialize_mail'){
			// control if the email from the VARS_HTML is valid
			$isEmailValid = $this->controlEmail($emailUserForgot);
			if($isEmailValid){
				// select the user in the db
				$this->selectAdminForgotPassord();
				//put db user data in variables
				if($this->result['select_admin_forgot_pass'] != NULL){
					$nbState = 0;
					$emailUserFromDb = $this->result['select_admin_forgot_pass'][0]['email_user'];
					$userNameFromDb = $this->result['select_admin_forgot_pass'][0]['name_user'];
					$idUserFromDb = $this->result['select_admin_forgot_pass'][0]['id_user'];
					//verify if the email exist, if yes crypt the id and send the email 
					$this->result['nb_form_error'] = $nbState;
					$this->result['error_message'] = '*Cet administrateur est valide';
					// $cryptAdminId = Utils::crypter('rps042019.', $idUserFromDb);// to re-active to encrypt
					$cryptAdminId = $idUserFromDb;
					$this->mailForgot($userNameFromDb, $emailUserFromDb, $cryptAdminId);
				}
				else{
					$nbState = 4;
					$this->result['nb_form_error'] = $nbState;
					$this->result['error_message'] = '*Cet administrateur est inactif ou n\'existe pas!';
				}
			}
		}
	}

	/**
	 * Called in main function and take filled email as parameter
	 * Verify if email is filled and if it is a valid email
	 * return an error number
	 *
	 * @param string $emailToCompare
	 * @return integer $nbState
	 */
	private function controlEmail($emailToCompare){
		error_log('controlEmail function started');
		$nbState ='';
		$isEmailValid = false;
		$isEmailEmpty = true;
		$isEmailEmpty = !(isset($emailToCompare)) || $emailToCompare == '';
		if(!$isEmailEmpty){
			$isEmailValid = filter_var($emailToCompare, FILTER_VALIDATE_EMAIL);
			if($isEmailValid){
				$nbState = 1;
				$this->result['nb_form_error'] = $nbState;
				$this->result['error_message'] = '*L\'email est valide mais n\'existe pas en base de donnée';
			
			}
			else{
				$nbState = 3;
				$this->result['nb_form_error'] = $nbState;
				$this->result['error_message'] = '*L\'email est invalide';
			}
		}
		else{
			$nbState = 2;
			$this->result['nb_form_error'] = $nbState;
			$this->result['error_message'] = '*Le champ mail est vide';
		}
		error_log('isEmailValid:'.$nbState);
		return $nbState;
	}


	 /**
	  * Send an email with a link to a given email
	  *
	  * @param string $userName
	  * @param string $emailUser
	  * @param string $cryptAdminId
	  * @return void
	  */
	private function mailForgot($userName, $emailUser, $cryptAdminId){
		$mail = $emailUser;
		$subject = 'Ré-initialisation de votre mot de passe';
		$message ='<html>
						<head>
							<title>Ré-initialisation de votre mot de passe</title>
						</head>
						<body>
						<p>'.$userName.', vous avez oublié votre mot de passe</p>
						<p>Suivez le lien ci-dessous pour le réinitialiser : </p>
						<a href="http://localhost/rps/route.php?page=forgot&crypted_key='.$cryptAdminId.'">Cliquer ici pour modifier votre mot de passe!</a>
						</body>
					</html>';
		$isSent= mail($mail, $subject, $message);
		if($isSent){
			error_log('email envoyé');
		}
	}
//End of class
}
?>