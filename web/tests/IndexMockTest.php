<?php
/**
 * updated 13/09/2019
 */
require '../../modules/rps/index.php';
use PHPUnit\Framework\TestCase;
/**
 * Class IndexMockTest | file IndexMockTest.php
 *
 * In this class, we find all tests about the class Index.php
 *
 * List of classes needed for this class
 * index.php
 *
 * @package Rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */
class IndexMockTest extends TestCase{

    /**
     * Init of global variables
     */
    private $stub;
    private $isUserValidFromBdd;
    private $isCodeValidFromRequest;

    /*
    * Set fixtures and stubs for all the class 
    **/
    protected function setUp() : void
    {
        $this->stub = $this->createMock(Index::class);
        $this->isUserValidFromBdd = true;
        $this->isCodeValidFromRequest = true;
    }

    /**
    * @test
    */
	function testCheckUserFromBdd(){
    
        // Configure the stub
        $this->stub->method('checkUserFromBdd')
            ->willReturn($this->isUserValidFromBdd);
        //assertion
        $this->assertTrue($this->stub->checkUserFromBdd());
    }

    /**
    * @test
    */
    public function testControlCodeFromRequest()
    {
        // Configure the stub
        $this->stub->method('controlCodeFromRequest')
            ->willReturn($this->isCodeValidFromRequest);

        $this->assertSame(true, $this->stub->controlCodeFromRequest());
    }
}


?>
