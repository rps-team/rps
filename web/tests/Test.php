<?php
require '../src/admin.php';

use PHPUnit\Framework\TestCase;


class Test extends TestCase{

    function hasPasswordsEmpty(){
        $verifyPasswords = new Admin();
        $this->assertSame($verifyPasswords->controlPassword('',''), '4');
    }
    function hasPasswordsEqual(){
        $verifyPasswords = new Admin();
        $this->assertSame($verifyPasswords->controlPassword('cyril@gamil.com','cyril.vassallo@gamil.com'), '5');
    }
    function hasPasswordsValidSyntax(){
        $verifyPasswords = new Admin();
        $this->assertSame($verifyPasswords->controlPassword('cyrilgamil.com','cyrilgmail.com'), '6');
    }

    function hasPasswordsValid(){
        $verifyPasswords = new Admin();
        $this->assertSame($verifyPasswords->controlPassword('cyril@gamil.com','cyril@gmail.com'), '0');
    }
}

?>
