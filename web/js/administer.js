/**
 * MAJ 25/05/2019
 * 
 * js administer  | file administer.php
 *
 * In this js files, we manage adding and editing administrator on fly 
 *      in the view "administer.html".
 *
 *
 * List of files working with
 * administer.css 
 * administer.html
 * administer.php
 * admin_service.php
 *  
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */

/**
 * Called when load view is finished
 * Set the title of the page
 * move interface buttons to admin_header.html
 */
$(window).on('load', function() {
    $('.page-title').html('Administrer');
    moveInterfaceButtonToHeader();
    var currentTime = Math.floor(Date.now() / 1000);
    elemCurrentTime = document.getElementById('current_time');
    elemCurrentTime.value = currentTime;
})


    /**
    * 
    * Called when onclick event on 'Ajouter' or 'Modifier' button
    * It take add or edit parameter according to 'Ajouter or 'Modifier' button 
    * init variable boolean and clear all field
    * Check what mode add or edit is asked
    * Verify the passwords fields state (empty or not)
    * if add do all controls function
    * if edit only do all controls function
    * if edit and passwords field are both empty do not do password control
    * Then
    * if readyAddAdmin = true do addAdmin() ajax
    * if readyEditAdmin = true do updateAdmin() ajax
    * 
    * 
    * @param string addOrEditMode 
    * 
    */
function modifyListOfAdmin(addOrEditMode) {
    clearHighlighted();
    var isNameEmpty = true;
    var isPasswordValid = false;
    var isEmailValid = false;
    var readyAddAdmin = false;
    var readyEditAdmin = false;
    var isPasswordAreEmpty = $('#password_user').val() == '' &&
        $('#password_confirm_user').val() == '';

    if (addOrEditMode == 'add') {
        isPasswordValid = controlPassword();
        isEmailValid = controlEmail();
        isNameEmpty = controlName();
        readyAddAdmin = !isNameEmpty && isEmailValid && isPasswordValid;
        console.log('ready add Admin: ' + readyAddAdmin);
        if (readyAddAdmin) {
            addAdmin(); // ajax
        }
    } else if (addOrEditMode == 'edit') {
        isEmailValid = controlEmail();
        isNameEmpty = controlName();
        console.log('is Password Fields Are Empty: ' + isPasswordAreEmpty)
        if (isPasswordAreEmpty) {
            readyEditAdmin = !isNameEmpty && isEmailValid;
        } else {
            isPasswordValid = controlPassword();
            readyEditAdmin = !isNameEmpty && isEmailValid && isPasswordValid;
        }
        console.log('ready edit admin: ' + readyEditAdmin);
        if (readyEditAdmin) {
            updateAdmin(); // ajax
        }
    }
}

/**
 * Called in function addAdmin
 * Get values in field name_user and email_user
 * Get switch right sate
 * Get the index of of the next row to creat
 * Select the table
 * Create the tr of the admin and set the id
 * insert the tr of the admin into the DOM
 * get the pattern from the constructAdminRow function
 * insert in tr the pattern sHtml into the DOM
 **/
function displayNewAdmin() {
    var name_user = $('#name_user').val();
    var email_user = $('#email_user').val();
    //give the admin boolean right which was selected by the switch Droits Limités, TRUE = isLimitedAdmin
    var isLimitedAdmin = giveSelectedRightOfAdmin();
    console.log('is limited: ' + isLimitedAdmin);
    //give the max number of an existing same class
    var indexOfAdminRowToAdd = giveClassLength('usr-admin-row');
    console.log('Their are' + indexAddAdmin + ' admin.');
    //select the table
    var table = $('#usr-table');
    //construct and instal the row with the right index
    var newTr = $('<tr class="usr-admin-row" id="usr-admin-row_' + indexOfAdminRowToAdd + '">');
    $(table).append(newTr);
    //construct and add the content of the  AdminRow with the right index and the type of admin
    var sHtml = constructAdminRow(isLimitedAdmin, indexOfAdminRowToAdd, name_user, email_user)
    $(newTr).append(sHtml);
    var indexAddAdmin = giveClassLength('usr-admin-row');
    console.log('Theire are' + indexOfAdminRowToAdd + ' admin.');

}

/**
 * Called in function updateAdmin
 * Get values in field name_user and email_user
 * Get switch right sate
 * Get the index of the admin row to modify
 * Select the table
 * Create the tr of the admin and set the id
 * insert the tr of the admin into the DOM
 * get the pattern from the constructAdminRow function
 * insert in tr the pattern sHtml into the DOM
 * Select and modify by replacing the rowToModify by newTr
 **/
function displayExistingAdmin() {
    var name_user = $('#name_user').val();
    var email_user = $('#email_user').val();
    //give the admin right which was selected by the switch Droits Limités
    var isLimitedAdmin = giveSelectedRightOfAdmin();
    console.log('is limited: ' + isLimitedAdmin);
    //give the id has to be modify by data-edit property which was insert by editAdmin fuction on the edit button  
    var rowToModify = $('#usr-btn-edit').data('edit');
    //find with a regEx function the index of the row has to be modify
    var indexOfAdminToEdit = findNumbersInId(rowToModify);
    console.log('Indice Admin: ' + indexOfAdminToEdit);
    //select the table
    var table = $('#usr-table');
    //construct and instal the row with the right index
    var newTr = $('<tr class="usr-admin-row" id="usr-admin-row_' + indexOfAdminToEdit + '">');
    $(table).append(newTr);
    //construct and edit the content of the AdminRow with the right index and the type of admin
    var sHtml = constructAdminRow(isLimitedAdmin, indexOfAdminToEdit, name_user, email_user)
    $(newTr).append(sHtml);
    $(rowToModify).replaceWith(newTr);
    console.log($(rowToModify).replaceWith(newTr));
}


/**
* 
* Called in displayExistingAdmin and displayNewAdmin,
* take as argument:
* isLimitedAdmin, indexOfAdminRow, name_user, email_user
* construct the pattern of an administrator row
* set and hidden id column
* change color and icon if isLimitedAdmin or not
* do not set active and delete icon on the first id = 1 (Super Administrator)
* return a sting Html as pattern
* 
* @param boolean isLimitedAdmin 
* @param integer indexOfAdminRow 
* @param string name_user 
* @param string email_user 
*/
function constructAdminRow(isLimitedAdmin, indexOfAdminRow, name_user, email_user) {
    var id_user = getUserId(indexOfAdminRow);
    var isSuperAdmin = id_user == 1;
    console.log('isSuperAdmin: ' + isSuperAdmin);
    var sHtml = '';
    sHtml += '<td id="usr-id-admin_' + indexOfAdminRow + '" hidden>' + id_user + '</td>';
    if (isLimitedAdmin) {
        sHtml += '<td style="color:#1F85DE" id="usr-right-admin_' + indexOfAdminRow + '">Limité</td>';
        sHtml += '<td style="color:#1F85DE" id="usr-name-admin_' + indexOfAdminRow + '">' + name_user + '</td>';
        sHtml += '<td style="color:#1F85DE" id="usr-email-admin_' + indexOfAdminRow + '">' + email_user + '</td>';
    } else {
        sHtml += '<td style="font-size:25px;color:#DE1F85" id="usr-right-admin_' + indexOfAdminRow + '"><i class="fas fa-user-cog icon-nav usr-admn-icon"></i></td>';
        sHtml += '<td style="color:#DE1F85" id="usr-name-admin_' + indexOfAdminRow + '">' + name_user + '</td>';
        sHtml += '<td style="color:#DE1F85" id="usr-email-admin_' + indexOfAdminRow + '">' + email_user + '</td>';
    }
    sHtml += '<td onclick="editAdmin(' + indexOfAdminRow + ')"><i class="fas fa-pencil-alt" style="font-size:25px;color:#DE1F85"></i></td>';
    if (!isSuperAdmin) {
        sHtml += '<td><div class="custom-control custom-switch"><input onclick="activeAdmin(' + indexOfAdminRow + ')" type="checkbox" class="custom-control-input" id="usr-active-admin_' + indexOfAdminRow + '" checked><label class="custom-control-label" for="usr-active-admin_' + indexOfAdminRow + '"></label></div></td>';
        sHtml += '<td onclick="setModalContent(' + indexOfAdminRow + ')" data-toggle="modal" data-target="#usr-delete-modal"><i class="fas fa-trash-alt" style="font-size:25px;color:#DE1F85"></i></td>';
    }
    return sHtml;
}

/**
 * Called when onclick event on 'Ajout d'un Administrateur' button in DOM,
 * Clear all fields
 * Clear all highlight
 * Display the 'Nouvel Administrator' form box
 **/
function createNewAdmin() {
    clearTextFields();
    clearHighlighted();
    $('#usr-btn-edit').hide();
    $('#usr-btn-add').show();
    $('#usr-addEditBox').hide();
    $('#usr-box-title').html('Nouvel Administrateur');
    $('#usr-addEditBox').fadeIn(2000);
    scrollToAddEditBox()
}


/**
 * Called when onclick event on pencil icon in DOM,
 * take as argument the number of the administrator row witch is clicked,
 * Create data-edit attribute contain the usr-admin-row clicked
 * Set the data-edit attribute on the button 'Modifier' element
 * pickup in the DOM all data one administrator row:
 * id_user,
 * name_user,
 * email_user,
 * right_user
 * Set the switch 'Droit Limité' in potion depends of the administrator right , 
 * Display the 'Modifier Administrator' elements (Tile, edit btn, show, hide)
 * Set get values in field of the displayed form box 'Modifier Administrateur'
 * Scroll to the appeared form box
 * 
 * @param integer index 
 * 
 **/
function editAdmin(index) {
    clearHighlighted();
    var userAdminRow = '#usr-admin-row_' + index
    $('#usr-btn-edit').data('edit', userAdminRow);

    var id_user = $('#usr-id-admin_' + index).text();
    var name_user = $('#usr-name-admin_' + index).text();
    var email_user = $('#usr-email-admin_' + index).text();
    var right_user = $('#usr-right-admin_' + index).text();
    right_user = right_user.toLowerCase();
    console.log(right_user);
    if (right_user == "limité") {
        $("#switches-right").prop('checked', true);
    } else {
        $("#switches-right").prop('checked', false);
    }

    $('#usr-btn-add').hide();
    $('#usr-btn-edit').show();
    $('#usr-addEditBox').hide();
    $('#usr-box-title').html('Modifier Administrateur');
    $('.error-info').text('Vous pouvez effectuer les modifications sans saisir le mot de passe, dans ce cas le mot de passe actuel sera conservé pour l\’administrateur modifié.').css({ 'color': 'blue', 'font-weight': 'bolder' });
    $('#usr-addEditBox').fadeIn(2000);
    $('#id_user').val(id_user);
    $('#name_user').val(name_user);
    $('#email_user').val(email_user);
    scrollToAddEditBox()
}


/**
 * Called in modifyListOfAdmin function 
 * Check if name_user is provided by user
 **/
function controlName() {
    console.log('START function controlName');
    var name_user = $('#name_user').val();
    var isNameEmpty = false;
    if (name_user == '') {
        isNameEmpty = true
        $('#name_user').css('border', '2px solid red');
        $('.error-info').text('*Le champ nom de l\'utilisateur est vide')
    }
    return isNameEmpty;
}


/**
 * Called in modifyListOfAdmin function 
 * Check if email_user is provide by user and valid with a RegExp object
 **/
function controlEmail() {
    console.log('START function controlEmail');
    var result = false;
    var isEmpty = $('#email_user').val() == '';
    var emailToCompare = $('#email_user').val();
    if (!isEmpty) {
        var isValid = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');
        result = isValid.test(emailToCompare);
        if (result) {
            console.log('email Valid');
        } else {
            console.log('email incorrect type');
            $('#email_user').css('border', '2px solid red');
            $('.error-info').text('*L\'email entré est invalide');
        }
    } else {
        $('#email_user').css('border', '2px solid red');
        $('.error-info').text('*Le champ email de l\'utilisateur est vide');
        console.log('email vide');
        result = false;
    }
    return result;
}

/**
 * Called when  'Envoyer un code' is submit
 * Check if mail_to is provide by user and valid with a RegExp object
 * if not good don't submit the post
 **/
function controlMailTo() {
    console.log('START function controlEmail');
    var isToPost = false;
    var isEmpty = $('#mail_to').val() == '';
    var emailToCompare = $('#mail_to').val();
    if (!isEmpty) {
        var isValid = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');
        isToPost = isValid.test(emailToCompare);
        if (isToPost) {
            console.log('email Valid');
        } else {
            console.log('email incorrect type');
            $('#mail_to').css('border', '2px solid red');
            $('.error-info').text('*L\'email entré est invalide');
        }
    } else {
        $('#mail_to').css('border', '2px solid red');
        $('.error-info').text('*Le champ email de l\'utilisateur est vide');
        console.log('email vide');
        isToPost = false;
    }
    return isToPost;
}



/** 
 * Called in modifyListOfAdmin function,
 * Check:
 *      both password field are empty or not
 *      the equality of password_user and password_confirm_user
 *      if it is a valid password type with RegExp object.
 * Return result as boolean:
 * if password valid result = true
 * else result = false
 **/
function controlPassword() {
    console.log('START function controlPassword');
    var password = $('#password_user').val();
    var confirm = $('#password_confirm_user').val();
    var isEmpty = $('#password_user').val() == '' || $('#password_confirm_user').val() == '';
    if (!isEmpty) {
        var result, isEqual, regPassword, isPasswordValid;
        isEqual = password == confirm;
        if (isEqual) {
            regPassword = new RegExp('^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[-+!\.*$@%_])([-+!\.*$@%_\\w]{8,15})$'); // Carefully to escape back slashes
            console.log('inside function password: ' + regPassword);
            isPasswordValid = regPassword.test(password);
            console.log(isPasswordValid);
            if (isPasswordValid) {
                console.log('password Valid');
                result = true;
            } else {
                console.log('password invalid type');
                $('#password_user').css('border', '2px solid red');
                $('#password_confirm_user').css('border', '2px solid red');
                $('.error-info').text('*Vérifiez le mot de passe. Un mot de passe valide  doit contenir 8 à 15 caractères, au moins une lettre minuscule, au moins une lettre majuscule,au moins un chiffre, au moins un de ces caractères spéciaux: . $ @ % * + - _ !  et aucun autre caractère possible');
                result = false;
            }
        } else {
            console.log('password are not sames');
            $('#password_user').css('border', '2px solid red');
            $('#password_confirm_user').css('border', '2px solid red');
            $('.error-info').text('*Les mots de passe sont différents');
            result = false;
        }
    } else {
        console.log('password field empty');
        $('#password_user').css('border', '2px solid red');
        $('#password_confirm_user').css('border', '2px solid red');
        $('.error-info').text('*Champ mot de passe vide');
        result = false;
    }
    return result;
}


/**
 * Called in displayExistingAdmin function
 * Take a string id name as argument
 * extract a number of id using match() method and regex as argument
 * return the number extracted 
 * 
 * @param integer sId 
 * 
**/
function findNumbersInId(sId) {
    var numberInId = sId.match(/\d+/g).join('')
    return numberInId;
}

/**
 * Called in displayNewAdmin function
 * Take a string class name as argument
 * return a same class length number
 * 
 * @param string sClassName 
 * 
 **/
function giveClassLength(sClassName) {
    classLength = $('.' + sClassName).length;
    return classLength;

}


/**
 * Called by displayNewAdmin and displayExistingAdmin function
 * return the actual state of switches-right in Modifier/ Ajouter Administrator box 
 **/
function giveSelectedRightOfAdmin() {
    isLimited = $('#switches-right').prop('checked');
    console.log('IS LIMITED BOOLEAN:' + isLimited);
    return isLimited;
}


/**
 * Relate to cancel button, 
 * close add/edit box,
 * clear all input and display
 **/
function cancelEditAdmin() {
    $('#usr-addEditBox').fadeOut(1000);
    clearTextFields();
    clearHighlighted();
}

/**
 * Call clear functions
 **/
function eraseMailTo(){
    clearHighlighted();
    clearTextFields();
}


/**
 * Clear the four inputs
 * name_user,
 * email_user,
 * password_user,
 * password_confirm_user,
 * when is called
 **/
function clearTextFields() {
    $('#id_user').val('');
    $('#mail_to').val('');
    $('#name_user').val('');
    $('#email_user').val('');
    $('#password_user').val('');
    $('#password_confirm_user').val('');
    $('.error-info').text('').css('color', 'red');
    console.log('textfield cleared');
}



/**
 * Is Called when need to remove highlighted display red border and error
 **/
function clearHighlighted() {
    $('#mail_to').css('border', '');
    $('#name_user').css('border', '');
    $('#email_user').css('border', '');
    $('#password_user').css('border', '');
    $('#password_confirm_user').css('border', '');
    $('.error-info').text('').css('color', 'red');
}


/**
 * Called by delete button of the modal in the DOM,
 * remove an entire administrator from the administrator list in the DOM,
 * when press delete button in the modal,
 * (a pre-added attribute data-delete should correspond to an id row)
 **/

function removeAdmin() {
    var elementToDelete = $('#usr-btn-delete').data('delete');
    $(elementToDelete).remove();
}


/**
 * Called by constructAdminRow function,
 * get the user Id from the DOM at indexed row of admin,
 * return the id_user
 * 
 * @param integer index 
 * 
 **/
function getUserId(index) {
    var id_user;
    id_user = $('#usr-id-admin_' + index).html();
    console.log('Selected user Id: ' + id_user);
    return id_user;
}



/**
 * Called by onclick in the DOM and take index of the row as parameter,
 * set the content of the modal according to the selected trash for delete 
 * create a data-delete with the id of the row 
 * insert on usr-bt-delete a data-delete as attribute contain the row id to remove
 * insert on usr-bt-delete a data-id as attribute containing the id to delete
 * 
 * 
 * @param integer index 
 * 
 **/
function setModalContent(index) {
    var userAdminRow = '#usr-admin-row_' + index;
    var name_user = $('#usr-name-admin_' + index).html();
    var id_user = $('#usr-id-admin_' + index).html();
    var sContent = '';
    sContent += '<p>Vous êtes sur le point de supprimer <b>' + name_user + '</b> de la lite des utilisateurs de ce gestionnaire.</p>';
    sContent += '<p>Voulez vous confirmer ce choix?</p>';
    $('.modal-body').html(sContent);
    $('#usr-btn-delete').data('delete', userAdminRow);
    $('#usr-btn-delete').data('id', id_user);
}




// RELATED TO THE ANIMATION

/**
 * Called in  add/editAdmin function after box appear
 * It scroll to the box just appeared
 **/
function scrollToAddEditBox() {
    $('html, body').stop().animate({
        scrollTop: $('#usr-addEditBox')
            .offset().top
    }, 'slow');
}

/**
 * Called onclick on switch right in the DOM
 * it change the color of right label "Limité" in the DOM
 **/
function toggleColor() {
    var labelLimited = $('#usr-label-limite');
    $('#switches-right').prop('checked') ? labelLimited.css('color', '#1F85DE') : labelLimited.css('color', 'gray')
}

/**
 * Called on onclick event 
 * relate to the switch Envoyer un code et Gestion utilisateur
 **/
function codeUserSwitch() {

    elmtLinkUser = document.getElementById('blockLienUser');
    elmtLinkCode = document.getElementById('blockLienCode');
    elmtBlockUser = document.getElementById('blockUser');
    elmtBlockCode = document.getElementById('blockCode');
    elmtImgUser = document.getElementById('imgLienUser');
    elmtImgCode = document.getElementById('imgLienCode');

    if (elmtLinkUser.classList.contains('classHidden') == true) {
        elmtLinkUser.classList.remove('classHidden');
        elmtBlockUser.classList.remove('classHidden');
        elmtImgUser.classList.remove('classHidden');
        elmtLinkCode.classList.add('classHidden');
        elmtBlockCode.classList.add('classHidden');
        elmtImgCode.classList.add('classHidden');
    } else {
        elmtLinkUser.classList.add('classHidden');
        elmtBlockUser.classList.add('classHidden');
        elmtImgUser.classList.add('classHidden');
        elmtLinkCode.classList.remove('classHidden');
        elmtBlockCode.classList.remove('classHidden');
        elmtImgCode.classList.remove('classHidden');
    }
}

/**
 * Called on window load
 * move usr-div-button to the header-bloc
 **/
function moveInterfaceButtonToHeader() {
    var divButton = $('#usr-div-button');
    divButton.remove();
    $('#header-bloc').prepend(divButton);
    $('#usr-div-button').removeAttr('hidden');
}