
/**
 * MAJ 21/08/2019
 * 
 * js client | file client.js
 *
 * In this js files, we manage the view of the client page with javascript
 *
 * List of files working with
 * client.css
 * client.html
 * client.php
 * 
 *
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */



/**
 * Called when load view is finished
 * Set the title of the page
 */
$(window).on('load', function() {
    var surveyTitle = $('#cli-active-survey-title').html();
    $('#cli-active-survey-title').remove();
    $('.page-title').html(surveyTitle);
})



/**
 * 
 * That function is an automatic scroll 
 * 
 * @param integer id_Div 
 * 
 */
function scrollToDiv(id_Div){
    $('html, body').animate({scrollTop: $(id_Div).offset().top}, 'slow');
}



/**
 * This function show the survey section and hide others
 */
function showSurvey(){
    $('.cli-tooltip').attr('hidden','hidden');
    $('.cli-activity').attr('hidden','hidden');
    $('#survey').removeAttr('hidden');
    $('#btn-valid-activity').attr('hidden','hidden');
}

/**
 * This function show the submit block of the survey
 */
function displaySubmit()
{
    $('.cli-submit-bloc').removeAttr('hidden');
}


function collapseTheme(currentTheme){
    let actualTheme = $(currentTheme)
    actualTheme.removeClass('show');
    let nextTheme = $(currentTheme).next().next();
    nextTheme.addClass('show');
}

function collapseRubric(currentRubric){
    let actualRubric = $(currentRubric)
    actualRubric.removeClass('show');
    let nextRubric = $(currentRubric).parent().next().children().next()
    nextRubric.addClass('show').removeClass('hide');

}