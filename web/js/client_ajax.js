/**
 * MAJ 21/08/2019
 * 
 * js client_ajax  | file client_ajax.js
 *
 * In this js files you will find all functions ajax relative to client page
 *
 * List of files working with:
 * client.js
 * client_ajax.html
 * client_service.php
 * 
 *
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */




/**
 * Update user category and select all existing answers
 * 
/**/
function upCategory_selAnswers() {
    var datas = {
        bJSON: 1,
        page: 'client_ajax',
        mode: 'category_answers',
        id_type: $('#cli-job-list').val()
    }
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,

        })
        .done(function(result) {
            // alert('error : ' + result[0]['error']);
            var selectAllAnswersOfRubric = result[0]['selectAnswers'];
            for (var keyMainArray in selectAllAnswersOfRubric){
                var lineMainArray = selectAllAnswersOfRubric[keyMainArray];
                var idQuestion ="";
                // For each elements set in DOM
                for(var keyElement in lineMainArray){
                    switch(keyElement){
                        case 'id_question':
                            idQuestion = lineMainArray[keyElement];
                        break;
                        case 'id_suggestion':
                            //ATTENTION special syntax to difference sames input radio ids by them value
                            $('input[type=radio][id=radioSuggestion_'+ idQuestion +'][value='+ lineMainArray[keyElement] +']').prop('checked', true)
                        break;
                        case 'comment':
                            $('#comment_'+idQuestion).html(lineMainArray[keyElement])
                        break;
                        default:
                    }
                }
            }
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });
}


/**
 * 
 * Save all rubric answers in a given theme
 * 
 * @param integer id_theme 
 * @param integer id_rubric 
 * 
 */
function saveAnswers(id_theme, id_rubric) {
    var sNameField, sValueField;
    var datas = {
        bJSON: 1,
        page: "client_ajax",
        mode: 'save_answers'
    }
    $("#div_" + id_theme + "_" + id_rubric + "  textarea, #div_" + id_theme + "_" + id_rubric + "  input").each(function() {
        sNameField = $(this).attr("id");
        // console.log($(this).val());
        // je protège au cas où que j'ai pas de id trouvé dans ma balise
        if ((typeof sNameField != "undefined") && (sNameField != "")) {
            if ($(this).attr("type") == "radio") {
                if (typeof $('#' + sNameField + ':checked').val() == "undefined") {
                    sValueField = "";
                } else {
                    sValueField = $('#' + sNameField + ':checked').val();
                }
            } else {
                sValueField = $(this).val();
            }
        }
        eval('datas.' + sNameField + '= "' + sValueField + '"');
    });
    // ici j'ai tout mon datas de rempli youpi !!
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,

        })
        .done(function(result) {
            // alert('error = ' + result[0]["error"]);
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });
}

/**
 * 
 * Ajax that confirm that the survey has been submitted by the user
 * 
 */
function confirmSurvey(){
    var datas = {
        bJSON: 1,
        page: 'client_ajax',
        mode: 'confirm_survey'
    }
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,

        })
        .done(function(result) {
            if(result[0]['error'] == 10){
                // console.log(result[0]['error']);
                $('.themes').hide();
                $('.cli-message-val').fadeIn(2000);
                $('.cli-thumbs-up').fadeIn(2000);
                $('.btn-pink').hide()
            }
            else if(result[0]['error'] == 11){
                // console.log(result[0]['error']);
                $('.themes').hide();
                $('.cli-message-edit').fadeIn(2000);
                $('.btn-pink').hide()
                $('.cli-thumbs-up').fadeIn(2000);
            }
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });
}



