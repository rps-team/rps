/**
 * MAJ 21/08/2019
 * 
 * js admin-ajax  | file admin-ajax.js
 *
 * In this js file, we manage all about ajax post of admin page
 * 
 *
 * List of files working with:
 * admin-ajax.html
 * admin-ajax.php
 * admin-ajax_service.php
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */

/**
 * 
 * Ajax to send email to an user
 * 
 */
function ajaxForReinitializeMail() {
    var datas = {
        bJSON: 1,
        page: 'admin_ajax',
        mode: 'initialize_mail',
        email_user_forgot: $('#adm_mail_forgot').val()
    }
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,

        })
        .done(function(result) {
            if(result[0]["error"]== 0){
                displayPop();
            }
            else if(result[0]["error"]== 1){
                console.log(result[0]["message"]);
            }
            else if(result[0]["error"]== 2){
                console.log(result[0]["message"]);
            }
            else if(result[0]["error"]== 3){
                console.log(result[0]["message"]);
            }
            else if(result[0]["error"]== 4){
                console.log(result[0]["message"]);
                $('.modal-info').text(result[0]["message"]);
            }
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });
}