/**
 * MAJ 21/08/2019
 * 
 * js home  | file home.js
 *
 * In this js files, we can all js  functions relative to Home page
 *
 * List of files working with
 * 
 * home.css
 * home.html
 * home.php
 *
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */

/**
 * Called when load view is finished
 * Set the title of the page
 */
$(window).on('load', function() {
    $('.page-title').html('Accueil');
})