/**
 * MAJ 21/08/2019
 * 
 * js survey  | file survey.js
 *
 * In this js files, we manage all about survey.html page
 * 
 * List of files working with
 * survey.css
 * survey.html
 * survey.php
 * 
 *
 * List of files working with:
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */


/**
 * 
 * Scroll to a given div
 * 
 * @param integer id_Div 
 */
function scrollToDiv(id_Div){
    $('html, body').stop().animate({scrollTop: $(id_Div).offset().top}, 'slow');
}

/**
 * 
 * Show hide
 * 
 */
function addSurvey()
{
    $('#sur-edit-survey').hide();
    $('#sur-add-survey').fadeIn(2000);
    
}

/**
 * 
 * Show hide
 * 
 */
function editSurvey()
{
    $('#sur-add-survey').hide();
    $('#sur-edit-survey').fadeIn(2000);;
}


/**
 * 
 * hide
 * 
 */
function cancelEditSurvey()
{
    $('#sur-edit-survey').fadeOut(1000);
    $('#sur-add-survey').fadeOut(1000);
    $('.form-group').trigger("reset");
    console.log("cancel")
}


/**
 * 
 * active import box
 * 
 */
function toggleImport(){
    $(".sur-choice-import").fadeToggle(2000);
}


/**
 * 
 * toggle network icons
 * 
 * @param integer indice_network 
 */
function toggleNet(indice_network)
{
    console.log("start instructions");
    if(document.getElementById("checkbox"+ indice_network).checked== true)
    {
        $('.sur-toggle-color').css("color", "gray");
        console.log(indice_network);
        console.log($("#network"+ indice_network));
        $("#network"+ indice_network).css("color","blue");
    }
    else if((document.getElementById("checkbox"+ indice_network).checked== false))
    {
        $('.sur-toggle-color').css("color", "gray");
    }
}
