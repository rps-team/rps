/**
 * MAJ 21/08/2019
 * 
 * js statistic  | file statistic.js
 *
 * In this js files, we manage all about statistic.html page
 * 
 * List of files working with
 * statistic.css
 * statistic.html
 * statistic.php
 * 
 *
 * List of files working with:
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */


/**
 * 
 * The PHP loop should construct on load arrays to load all chart when page load
 * 
*/
$(window).on('load', function() {
    $('.page-title').html('Resultats d\'enquête');
    let pieData0 = new Array(['jamais', 12], ['rarement', 92], ['souvent', 10], ['très souvent', 2]);
    creatDivForCharts('0', pieData0);
    let pieData1 = new Array(['jamais', 25], ['rarement', 12], ['souvent', 5], ['très souvent', 2]);
    creatDivForCharts('1', pieData1);
    let pieData2 = new Array(['jamais', 45], ['rarement', 19], ['souvent', 34], ['très souvent', 78]);
    creatDivForCharts('2', pieData2);
    let pieData3 = new Array(['jamais', 35], ['rarement', 9], ['souvent', 4], ['très souvent', 28]);
    creatDivForCharts('3', pieData3);
    let pieData4 = new Array(['jamais', 4], ['rarement', 19], ['souvent', 74], ['très souvent', 78]);
    creatDivForCharts('4', pieData4);
})



/**
 * Generic function for charts
 * 
 * @param integer index 
 * @param array myDataPie 
 */
function creatDivForCharts(index, myDataPie){

    //les type: let chartTypes = new Array('line','bar','pie');
    let divCharts ="";
    let divChartLine= "";
    

    divCharts = document.getElementById('chart-'+ index);
    console.log(index);
    divChartLine = document.createElement("div");
    divChartLine.setAttribute('id', `chartcontainer_pie_`+index);
    divCharts.appendChild(divChartLine);


    let pieColors = new Array('#8cbf29', '#FFEB00', '#FF8A00','#DE1F1F');
    let myChartPie = new JSChart('chartcontainer_pie_'+index, 'pie');
    myChartPie.setDataArray(myDataPie);
    myChartPie.colorizePie(pieColors);
    myChartPie.setSize(250,250);
    myChartPie.setTitle("");
    myChartPie.set3D(false);
    myChartPie.draw(); 
    
}