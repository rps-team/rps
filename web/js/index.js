/**
 * MAJ 15/07/2019
 * 
 * js index  | file index.js
 *
 * In this js files, we manage all about index.html page
 * 
 *
 * 
 * List of files working with
 * index.css
 * index.html
 * index.php
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */


/**
 * Called when load view is finished
 * Set the title of the page
 */
$(window).on('load', function() {
    $('.page-title').html('Bienvenue');
})


/**
 * Control user code type
 * A password should contain 8 digits, number ['0-9'], letter [a-f] 
 */
function controlCode(){
    var isCodeValid = false;
    var code = $('#ind_password_user').val();
    code = code.toLowerCase();
    console.log(code);
    if(code.length == 8){
        var regCode = new RegExp('^[a-z0-9]+$'); 
        isCodeValid = regCode.test(code);
        console.log(isCodeValid);
    }else {
        var erroInfo = '<p class="ind-para-pink" style="font-weight: bolder;">Code invalide !!</p>';
        $('#ind_input_box').append(erroInfo);
        
    }

    return isCodeValid;
}