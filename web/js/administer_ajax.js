/**
 * MAJ 24/05/2019
 * 
 * js administer_ajax  | file administer_ajax.js
 *
 * In this js files, we manage ajax functions
 * With this js file, we'll be able add, update, delete if an administrator is active by ajax post using json
 *
 * List of files working with:
 * administer_ajax.html
 * adminster_ajax.php
 * admin_service.php
 * 
 *
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */




/**
 * Called in modifiyListOfAdmin function in administer.js files
 * It add to bdd an administrator using POST mehtode with ajax json 
 * It POST administrator informations provided by the user in inputs field of the form "Nouvel Administrateur"
 * administer_ajax.php cacth those information in VARS_HTML and treat it in the back-end
 * The back-end return to the page administer_ajax.html results as  "error" , and "lastInsertId"
 * If error != 0 highlight inputs field border and  display an error info message
 * If error = 0 display call displayNewAdmin() function to display new admin in user table of DOM and clear input fields
 * 
 * 
/**/
function addAdmin() {
    var datas = {
        bJSON: 1,
        page: 'administer_ajax',
        mode: 'add',
        id_type: ($('#switches-right').prop('checked')) ? 2 : 1,
        name_user: $('#name_user').val(),
        email_user: $('#email_user').val(),
        password_user: $('#password_user').val(),
        password_confirm_user: $('#password_confirm_user').val()
    }
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,

        })
        .done(function(result) {
            clearHighlighted();
            // alert('erreur = ' + result[0]["error"]);
            // alert('last_id = ' + result[0]["lastInsertId"]);
            if (result[0]["error"] == 0) {
                displayNewAdmin();
                var lastUserId = result[0]["lastInsertId"];
                var indexOftheRow = giveClassLength('usr-admin-row') - 1;
                $('#usr-id-admin_' + indexOftheRow).html(lastUserId);
                clearTextFields();
                clearHighlighted();
            } else if (result[0]["error"] == 1) {
                $('#name_user').css('border', '2px solid red');
                $('.error-info').text('Le champ nom de l\'utilisateur est vide');
            } else if (result[0]["error"] == 2) {
                $('#email_user').css('border', '2px solid red');
                $('.error-info').text('Le champ email de l\'utilisateur est vide');
            } else if (result[0]["error"] == 3) {
                $('#email_user').css('border', '2px solid red');
                $('.error-info').text('L\'email entré est invalide');
            } else if (result[0]["error"] == 4) {
                $('#password_user').css('border', '2px solid red');
                $('#password_confirm_user').css('border', '2px solid red');
                $('.error-info').text('Champ mot de passe vide');
            } else if (result[0]["error"] == 5) {
                $('#password_user').css('border', '2px solid red');
                $('#password_confirm_user').css('border', '2px solid red');
                $('.error-info').text('Les mots de passe sont différents');
            } else if (result[0]["error"] == 6) {
                $('#password_user').css('border', '2px solid red');
                $('#password_confirm_user').css('border', '2px solid red');
                $('.error-info').text('Vérifiez le mot de passe. Un mot de passe valide  doit contenir 8 à 15 caractères, au moins une lettre minuscule, au moins une lettre majuscule,au moins un chiffre, au moins un de ces caractères spéciaux: . $ @ % * + - _ !  et aucun autre caractère possible');
            }else if (result[0]["error"] == 7) {
                $('#email_user').css('border', '2px solid red');
                $('.error-info').text('Cet email est déjà utilisé par un autre administrateur !');
            }
            
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });
}


/**
 * Called in modifiyListOfAdmin function in administer.js files
 * It add to bdd an administrator using POST mehtode with ajax json 
 * It POST changed administrator informations provided by the user in inputs field of the form "Nouvel Administrateur"
 * administer_ajax.php cacth those information in VARS_HTML and treat it in the back-end
 * The back-end return to the page administer_ajax.html results as  "error" , and "lastInsertId"
 * If error != 0 highlight inputs field border and  display an error info message
 * If error = 0 call displayExistingAdmin() function to display changed informations and clear input fields
 * 
/**/
function updateAdmin() {
    var datas = {
        bJSON: 1,
        page: 'administer_ajax',
        mode: 'edit',
        id_user: $('#id_user').val(),
        id_type: ($('#switches-right').prop('checked')) ? 2 : 1,
        name_user: $('#name_user').val(),
        email_user: $('#email_user').val(),
        password_user: ($('#password_user').val() != "") ? $('#password_user').val() : "",
        password_confirm_user: ($('#password_confirm_user').val() != "") ? $('#password_confirm_user').val() : ""
    }
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,
        })
        .done(function(result) {
            clearHighlighted();
            // alert('erreur = ' + result[0]["error"]);
            if (result[0]["error"] == 0) {
                displayExistingAdmin();
                cancelEditAdmin()
                clearHighlighted();
            } else if (result[0]["error"] == 1) {
                $('#name_user').css('border', '2px solid red');
                $('.error-info').text('Le champ nom de l\'utilisateur est vide');
            } else if (result[0]["error"] == 2) {
                $('#email_user').css('border', '2px solid red');
                $('.error-info').text('Le champ email de l\'utilisateur est vide');
            } else if (result[0]["error"] == 3) {
                $('#email_user').css('border', '2px solid red');
                $('.error-info').text('L\'email entré est invalide');
            } else if (result[0]["error"] == 4) {
                $('#password_user').css('border', '2px solid red');
                $('#password_confirm_user').css('border', '2px solid red');
                $('.error-info').text('champ mot de passe vide');
            } else if (result[0]["error"] == 5) {
                $('#password_user').css('border', '2px solid red');
                $('#password_confirm_user').css('border', '2px solid red');
                $('.error-info').text('Les mots de passe sont différents');
            } else if (result[0]["error"] == 6) {
                $('#password_user').css('border', '2px solid red');
                $('#password_confirm_user').css('border', '2px solid red');
                $('.error-info').text('Vérifiez le mot de passe. Un mot de passe valide  doit contenir 8 à 15 caractères, au moins une lettre minuscule, au moins une lettre majuscule,au moins un chiffre, au moins un de ces caractères spéciaux: . $ @ % * + - _ !  et aucun autre caractère possible');
            }
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });

}


/**
 * Called in modifyListOfAdmin function in administer.js files
 * It delete an administrator selected by onclick on the trash using ajax post
 * 
/**/
function deleteAdmin() {
    var datas = {
        bJSON: 1,
        page: 'administer_ajax',
        mode: 'delete',
        id_user: $('#usr-btn-delete').data('id')
    }
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,

        })
        .done(function(result) {
            // alert('error = ' + result[0]["error"]);
            removeAdmin();
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });
}




/**
 * 
 * Called in modifyListOfAdmin function in administer.js files
 * It activate or no an administrator using ajax post
 * @param integer index 
 */
function activeAdmin(index) {
    var datas = {
        bJSON: 1,
        page: 'administer_ajax',
        mode: 'active',
        id_user: $('#usr-id-admin_' + index).html(),
        active_user: ($('#usr-active-admin_' + index).prop('checked')) ? 1 : 0
    }
    $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false,

        })
        .done(function(result) {
            // alert('error = ' + result[0]['error']);
            removeAdmin();
        })
        .fail(function(err) {
            alert('error : ' + err.status);
        });
}