/**
 * MAJ 21/08/2019
 * 
 * js admin  | file admin.js
 *
 * In this js files, we manage all about admin page
 * verify fields and inputs data before submit
 * 
 *
 * List of files working with:
 * admin.html
 * admin.php
 * admin_service.php
 * 
 *
 * @package rps Project
 * @subpackage rps
 * @author @Afpa Lab Team 5
 * @copyright  1920-2080 The Afpa Lab Team 5 Group Corporation World Company
 * @version v1.0
 */


/**
 * Called when load view is finished
 * Set the title of the page
 */
$(window).on('load', function () {
    $('.page-title').html('Connexion');
})

/**
 * This function clear fields and launch checkData
 * 
 */
function verify_fields() {
    var isToSubmit = false;
    clearHighlighted();
    isToSubmit = checkData();
    return isToSubmit;
}

/**
 * This function call controlEmail and controlPassword functions
 * Return true if can submit
 * 
 */
function checkData() {
    var isToSubmit = false;
    var isEmailValid = false;
    var isPasswordValid = false;
    isPasswordValid = controlPassword();
    isEmailValid = controlEmail();
    if (isEmailValid && isPasswordValid) {
        isToSubmit = true;
    }
    return isToSubmit;
}

/**
 * Check if email fields is empty or if it is a valid type with a regEx
 * */
function controlEmail() {
    console.log('START function controlEmail');
    var result = false;
    var isEmpty = $('#adm-email').val() == '';
    var emailToCompare = $('#adm-email').val();
    if (!isEmpty) {
        var isValid = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');
        result = isValid.test(emailToCompare);
        if (result) {
            console.log('email Valid');
        } else {
            console.log('email incorrect type');
            $('#adm-email').css('border', '2px solid red');
            $('.error-info').text('L\'email entré est invalide');
        }
    } else {
        $('#adm-email').css('border', '2px solid red');
        $('.error-info').text('Le champ email de l\'utilisateur est vide');
        console.log('email vide');
        result = false;
    }
    return result
}

/** 
 *
 * Check:
 *      both password field are empty or not
 *      the equality of password_user and password_confirm_user
 *      if it is a valid password type with RegExp object.
 * Return result as boolean:
 * if password valid result = true
 * else result = false
 * */
function controlPassword() {
    console.log('START function controlPassword');
    var password = $('#adm-password').val();
    var isEmpty = $('#adm-password').val() == '' || $('#password_confirm_user').val() == '';
    if (!isEmpty) {
        var result, regPassword, isPasswordValid;
        regPassword = new RegExp('^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[-+!\.*$@%_])([-+!\.*$@%_\\w]{8,15})$'); // attetion les anti-slash sont échapés
        console.log('inside function password: ' + regPassword);
        isPasswordValid = regPassword.test(password);
        console.log(isPasswordValid);
        if (isPasswordValid) {
            console.log('password Valid');
            result = true;
        } else {
            console.log('password invalid type');
            $('#adm-password').css('border', '2px solid red');
            $('#password_confirm_user').css('border', '2px solid red');
            $('.error-info').text('Email ou mot de passe invalide');
            result = false;
        }

    } else {
        console.log('password field empty');
        $('#adm-password').css('border', '2px solid red');
        $('#password_confirm_user').css('border', '2px solid red');
        $('.error-info').text('Champ mot de passe vide');
        result = false;
    }
    return result;
}

/** 
 * Called when  'Envoyer un code de reinitialisation' btn from modal is submit
 * Check if adm_mail_forgot is provide by user and valid with a RegExp object
 * if not good don't submit the post a affect the modal with  an error message
 **/
function controlMailForgot() {
    console.log('START function controlMailForgot');
    var isToPost = false;
    var isEmpty = $('#adm_mail_forgot').val() == '';
    var emailToCompare = $('#adm_mail_forgot').val();
    if (!isEmpty) {
        var isValid = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');
        isToPost = isValid.test(emailToCompare);
        if (isToPost) {
            console.log('email Valid');
            ajaxForReinitializeMail();
        } else {
            console.log('email incorrect type');
            $('#adm_mail_forgot').css('border', '2px solid red');
            $('.modal-info').text('*L\'email entré est invalide').css('color', 'red');
        }
    } else {
        $('#adm_mail_forgot').css('border', '2px solid red');
        $('.modal-info').text('*Le champ email de l\'utilisateur est vide').css('color', 'red');
        console.log('email vide');
        isToPost = false;
    }
    return isToPost;
}

/**
 * Clear fields and highlighted
 **/
function eraseAll() {
    clearHighlighted();
    clearTextFields();
}


/**
 * Clear inputs
 * adm_mail_forgot
 * error-info
 **/
function clearTextFields() {
    $('#adm_mail_forgot').val('');
    console.log('textfield cleared');
}

/** 
 * 
 * Clear all highlighted fields and messages
 * 
 * */
function clearHighlighted() {
    $('#adm-email').css('border', '');
    $('#adm-password').css('border', '');
    $('#adm_mail_forgot').css('border', '');
    $('.error-info').text('').css('color', 'red');
    $('.modal-info').text('').css('color', 'red');
}


/** 
 * 
 * This function display pop message after submit
 * 
 * */
function displayPop() {
    element = $( "#adm-info-pop" );
    element.attr("class","w3-container w3-center w3-animate-left" ).show();
    console.log('adding class for animate');
}