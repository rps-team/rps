<?php
    //MAJ 04/06/2019
	//SESSION START
	session_start();
	require "configuration.php";
	$GLOBALS_INI= Configuration::getGlobalsINI();
	

	// Class dynamic
	if ((isset($_GET["page"])) && ($_GET["page"] != ""))	{
		$monPHP= $_GET["page"];
	}  else  {
		if ((isset($_POST["page"])) && ($_POST["page"] != ""))	{
			$monPHP= $_POST["page"];
		}  else  {
			$monPHP= "index";
		}
	}

    // list of classes authorized when you are NOT connected
    $list_class_disconnect = array("index","admin","forgot");
    // list of classes authorized when CONNECTED as user
    $list_class_user = array('index', 'client','client_ajax');
    // list of classes authorized when CONNECTED as limited admin
    $list_class_limited= array('index','admin', 'home', 'question','question_ajax', 'statistic','statistic_ajax');
    // list of classes authorized when CONNECTED as full admin
    $list_class_full= array('index','admin', 'home', 'question','question_ajax', 'statistic','statistic_ajax','survey','survey_ajax','administer','administer_ajax');

    if(!isset($_SESSION["id_user"]) ||(isset($_SESSION["id_user"])&& $_SESSION["id_user"]== ""))    {
        if(!in_array($monPHP, $list_class_disconnect))     {
            $monPHP = "index";
        }       
    }
    else if(isset($_SESSION["id_user"]) && $_SESSION["id_user"] != "" 
            && isset($_SESSION["user_type"]) && $_SESSION["user_type"] != "" 
            && isset($_SESSION["right_type"]) && $_SESSION["right_type"] != "" )  {
        
        error_log('CONNECTED');

        if ($_SESSION['user_type']== 'admin' && $_SESSION['right_type']==1) {
            error_log('FULL MODE');
            if(!in_array($monPHP, $list_class_full))    {
                $monPHP = "admin";
            }
        }
        else if ($_SESSION['user_type']== 'admin' && $_SESSION['right_type']==2) {
            error_log('LIMITED MODE');
            if(!in_array($monPHP, $list_class_limited)) {
                $monPHP = "home";
            }
        }
        else {
            error_log('USER MODE');
            if(!in_array($monPHP, $list_class_user))    {
                $monPHP = "index";
            }
        }   
    }
    

	
	// Test if classes exist
	if (!(file_exists($GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"] . $monPHP . ".php"))) {
		$monPHP= "index";
	}

	$myClass= ucfirst($monPHP);

	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"] . $monPHP . ".php"; 

	$oMain= new $myClass();



	$page_to_load= "route.html";
	//if AJAX WITH JSON
	if ((isset($oMain->VARS_HTML["bJSON"])) && ($oMain->VARS_HTML["bJSON"] == 1))	{
		$page_to_load= $monPHP . ".html";
	}

	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_FILES"] . $page_to_load;

	unset($oMain);
?>




